# Face animations module that handles all kinds of face animations

from stub_control import STUB_PYGAME
if STUB_PYGAME:
    from pygame_stubs import pygame
else:
    import pygame

import enum
import os
from threading import Lock
from logger import PrintToLog
from commands import Commands

BASE_IMAGE_FOLDER = os.path.dirname(os.path.realpath(__file__)) + "/face_animation_images"
BUBBLE_IMAGE_FOLDER = BASE_IMAGE_FOLDER + "/action_bubbles"

ALPHA_MIN = 0
ALPHA_MAX = 255
ALPHA_INC = 60

#First enum value acts as a command for the emotion, key in anim dict, and image file folder name
#Second enum value tells how many frames the emotion has in talking action
class FaceEmotions(enum.Enum):
    Happy = Commands.FACE.value[1][0], 7
    Neutral = Commands.FACE.value[1][1], 13
    Sleepy = Commands.FACE.value[1][2], 4
    Sad = Commands.FACE.value[1][3], 5

#Each face emotion has both static and talking states. Static means one still image from same sprite sheet that talking state uses.
#Static image is always the first frame from the sprite sheet.
class FaceActions(enum.Enum):
    Static = Commands.FACE.value[2][0]
    Talking = Commands.FACE.value[2][1]

#Bubble icons to give more visual info to user what the robot is doing.
#First value is the name of the icon to be used for a dictionary key.
#Second value is the name of the image file.
#Third value is the size of the image as a tuple.
#Fourth value is the location: left or right
class BubbleIcons(enum.Enum):
    Nothing = Commands.FACE_BUBBLE_ICON.value[0], "", (0, 0), ""
    Listening = Commands.FACE_BUBBLE_ICON.value[1], "listening.png", (178, 110), "left"
    Thinking = Commands.FACE_BUBBLE_ICON.value[2], "thinking.png",(178, 110), "left"
    Speech = Commands.FACE_BUBBLE_ICON.value[3], "speech.png", (120, 120), "right"
    Question = Commands.FACE_BUBBLE_ICON.value[4], "question.png", (120, 120), "right"

class FaceAnimations:
    def __init__(self, width, height):
        # Load each sprite sheet
        self.sheets = {}
        self.frameCounts = {}
        for face in FaceEmotions:
            self.sheets[face.value[0]] = pygame.image.load(BASE_IMAGE_FOLDER + "/" + face.value[0] + "/" + face.value[0] + "_talking_frames.png").convert()
            self.frameCounts[face.value[0]] = face.value[1]

        self.rect = pygame.Rect(0, 0, width, height)
        self.rectWidth = width
        self.rectXOffset = 0

        self.currentEmotion = FaceEmotions.Happy.value[0]
        self.currentAction = FaceActions.Static.value
        self.currentSheet = self.sheets[self.currentEmotion]

        self.bubbles = {}
        self.bubbleRects = {}
        for bubble in BubbleIcons:
            if bubble.value[0] != BubbleIcons.Nothing.value[0]:
                self.bubbles[bubble.value[0]] = pygame.image.load(BUBBLE_IMAGE_FOLDER + "/" + bubble.value[1]).convert()

                if bubble.value[3] == "left":
                    bubbleRectX = 0
                else: #right
                    bubbleRectX = width - bubble.value[2][0]

                self.bubbleRects[bubble.value[0]] = pygame.Rect(bubbleRectX, 0, bubble.value[2][0], bubble.value[2][1])

        self.currentBubble = BubbleIcons.Nothing.value[0]
        self.pendingBubble = self.currentBubble
        self.bubbleAlphaVal = ALPHA_MIN
        self.bubbleAlphaTarget = ALPHA_MIN

        self.mutex = Lock()
        PrintToLog("Face animations initialized")

    def SetFace(self, newFaceEmotion, newFaceAction):
        # What kind of face to draw
        if ((self.currentEmotion != newFaceEmotion) or (self.currentAction != newFaceAction)):
            if (any(x for x in FaceEmotions if x.value[0] == newFaceEmotion) and
                any(y for y in FaceActions if y.value == newFaceAction)):
                PrintToLog("Setting face: " + newFaceEmotion + ", " + newFaceAction)
                self.mutex.acquire()
                self.currentEmotion = newFaceEmotion
                self.currentAction = newFaceAction
                self.mutex.release()

    def SetBubble(self, newBubbleEnum):
        # What kind of bubble icon to draw in addition to a face
        newBubble = newBubbleEnum.value[0]
        if (self.currentBubble != newBubble):
            if any(bubble for bubble in BubbleIcons if bubble.value[0] == newBubble):
                PrintToLog("Setting bubble icon: " + newBubble)
                self.mutex.acquire()
                # Store the new bubble that will be shown after current bubble alpha transition is complete
                self.pendingBubble = newBubble
                self.mutex.release()

    def GetFace(self):
        self.mutex.acquire()
        emotion, action = self.currentEmotion, self.currentAction
        self.mutex.release()
        return (emotion, action)

    def Update(self):
        self.mutex.acquire()

        # Update face
        self.currentSheet = self.sheets[self.currentEmotion]

        if (self.currentAction == FaceActions.Static.value):
            if (self.rectXOffset != 0):
                self.rect.move_ip(-self.rectXOffset, 0)
                self.rectXOffset = 0
        else:
            offsetInc = 0
            if (self.rectXOffset < ((self.frameCounts[self.currentEmotion] - 1) * self.rectWidth)):
                self.rectXOffset = self.rectXOffset + self.rectWidth #total offset
                offsetInc = self.rectWidth #this time offset
            else:
                offsetInc = -self.rectXOffset #this time offset
                self.rectXOffset = 0 #total offset

            self.rect.move_ip(offsetInc, 0)

        # Update bubble
        if ((self.bubbleAlphaVal == self.bubbleAlphaTarget) and (self.currentBubble != self.pendingBubble)):
            # Current bubble has reached its target alpha and there is a new bubble waiting to be shown next
            if (self.bubbleAlphaVal == ALPHA_MAX):
                self.bubbleAlphaTarget = ALPHA_MIN # Start fading current bubble
            else:
                # Start to show new bubble
                self.currentBubble = self.pendingBubble
                self.bubbleAlphaTarget = ALPHA_MAX

        if (self.bubbleAlphaVal < self.bubbleAlphaTarget):
            self.bubbleAlphaVal = self.bubbleAlphaVal + ALPHA_INC
            if (self.bubbleAlphaVal > self.bubbleAlphaTarget):
                self.bubbleAlphaVal = self.bubbleAlphaTarget
        elif (self.bubbleAlphaVal > self.bubbleAlphaTarget):
            self.bubbleAlphaVal = self.bubbleAlphaVal - ALPHA_INC
            if (self.bubbleAlphaVal < self.bubbleAlphaTarget):
                self.bubbleAlphaVal = self.bubbleAlphaTarget

        if (self.currentBubble != BubbleIcons.Nothing.value[0]):
            self.bubbles[self.currentBubble].set_alpha(self.bubbleAlphaVal)

        self.mutex.release()

    def Draw(self, screen):
        self.mutex.acquire()
        screen.blit(self.currentSheet, (0, 0), self.rect)
        cb = self.currentBubble
        if (cb != BubbleIcons.Nothing.value[0]):
            screen.blit(self.bubbles[cb], self.bubbleRects[cb])
        self.mutex.release()

    def Cleanup(self):
        PrintToLog("Face animation cleanup")
