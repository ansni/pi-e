#Transform input voice file to text. Input and output are the same language.

from stub_control import STUB_GOOGLE_CLOUD
if STUB_GOOGLE_CLOUD:
    from google_cloud_stubs import speech_v1
    from google_cloud_stubs import enums
else:
    from google.cloud import speech_v1
    from google.cloud.speech_v1 import enums

from threading import Thread
from queue import Queue
from time import sleep
import os.path
from logger import PrintToLog

THIS_FILE_DIR = os.path.dirname(os.path.realpath(__file__))

UPDATE_PERIOD_S = 0.5

class QItem:
    def __init__(self, fileURI, Callback):
        self.fileURI = fileURI
        self.Callback = Callback

class SpeechToText:
    def __init__(self, languageCode):
        # Authenticate with Google Cloud account
        if not "GOOGLE_APPLICATION_CREDENTIALS" in os.environ:
            os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = THIS_FILE_DIR + "/google_access_token.json"

        self.client = speech_v1.SpeechClient()
        sampleRateHertz = 16000
        self.config = {"language_code": languageCode,
                       "sample_rate_hertz": sampleRateHertz, }

        self.queue = Queue()

        self.conversionThread = Thread(target = self.ConversionWorker, args = [])
        self.conversionThread.start()

        PrintToLog("Speech-to-text initialized")

    def ConversionWorker(self):

        while True:

            # Wait for requests from queue
            item = self.queue.get()
            if item is None:
                break #exit thread

            with open(item.fileURI, "rb") as f:
                content = f.read()

            audio = {"content": content}
            response = self.client.recognize(self.config, audio)
            text = ""
            for result in response.results:
                # First alternative is the most probable result
                alternative = result.alternatives[0]
                text = u"{}".format(alternative.transcript)
                break

            item.Callback(text)
            sleep(UPDATE_PERIOD_S) # Prevent CPU exhaustion if there are lots of requests

    def StartConversion(self, voiceFileURI, ConversionReadyCallback):
        fileFound = os.path.isfile(voiceFileURI)
        if fileFound:
            item = QItem(voiceFileURI, ConversionReadyCallback)
            self.queue.put(item)

    def Cleanup(self):
        self.queue.put(None)
        self.conversionThread.join()
        PrintToLog("Speech-to-text cleanup")
