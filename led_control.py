# Led control module

from stub_control import STUB_GPIO

if STUB_GPIO:
    import GPIO_stubs as GPIO
else:
    import RPi.GPIO as GPIO

import enum
import time
import gpio_pin_mapping_bcm as Pins
from threading import Thread, Lock
from logger import PrintToLog

LED_ON = GPIO.HIGH
LED_OFF = GPIO.LOW

UPDATE_PERIOD_S = 0.05

MAX_PWM_VALUE = 50
MIN_PWM_VALUE = 0

TOGGLING_ON_TIME_S = 0.5
TOGGLING_OFF_TIME_S = 0.5
PULSATING_INCREMENT = 8

class LedSelection(enum.Enum):
    LeftFrontLed = Pins.LEFT_LED_PIN
    RightFrontLed = Pins.RIGHT_LED_PIN

class LedStates(enum.Enum):
    Off = 0
    On = 1
    Toggling = 2
    Pulsating = 3

class LedControl:
    def __init__(self):
        self.running = True
        self.prevTime = time.time()
        self.currentState = LedStates.Off

        self.ledDict = {}
        for led in LedSelection:
            GPIO.setup(led.value, GPIO.OUT)
            pwmObj = GPIO.PWM(led.value, MAX_PWM_VALUE)
            pwmObj.start(0)

            self.ledDict[led] = {
                "state": LedStates.Off,
                "pwmObj": pwmObj,
                "pwmVal": MIN_PWM_VALUE,
                "timestamp": 0,
                "pulsatingInc": PULSATING_INCREMENT,
                "valChanged": False
            }

        self.mutex = Lock()
        self.periodicUpdateThread = Thread(target = self.PeriodicUpdate, args = [])
        self.periodicUpdateThread.start()
        PrintToLog("Led control initialized")

    def ChangeDutyCycleIfNeeded(self, led):
        ''' Internal function for LED control module '''
        if self.ledDict[led]["valChanged"]:
            self.ledDict[led]["valChanged"] = False
            newPwm = self.ledDict[led]["pwmVal"]
            self.ledDict[led]["pwmObj"].ChangeDutyCycle(newPwm)

    def UpdateOffState(self, led):
        if self.ledDict[led]["pwmVal"] != MIN_PWM_VALUE:
            self.ledDict[led]["pwmVal"] = MIN_PWM_VALUE
            self.ledDict[led]["valChanged"] = True

    def UpdateOnState(self, led):
        if self.ledDict[led]["pwmVal"] != MAX_PWM_VALUE:
            self.ledDict[led]["pwmVal"] = MAX_PWM_VALUE
            self.ledDict[led]["valChanged"] = True

    def UpdateTogglingState(self, led):
        currentPwmVal = self.ledDict[led]["pwmVal"]
        newPwmVal = currentPwmVal
        currentTime = time.time()

        if currentPwmVal == MIN_PWM_VALUE:
            if (currentTime - self.ledDict[led]["timestamp"] >= TOGGLING_OFF_TIME_S):
                self.ledDict[led]["timestamp"] = currentTime
                newPwmVal = MAX_PWM_VALUE
        else:
            if (currentTime - self.ledDict[led]["timestamp"] >= TOGGLING_ON_TIME_S):
                self.ledDict[led]["timestamp"] = currentTime
                newPwmVal = MIN_PWM_VALUE

        if (newPwmVal != currentPwmVal):
            self.ledDict[led]["pwmVal"] = newPwmVal
            self.ledDict[led]["valChanged"] = True

    def UpdatePulsatingState(self, led):
        currentPwmVal = self.ledDict[led]["pwmVal"]

        newPwmVal = currentPwmVal + self.ledDict[led]["pulsatingInc"]
        if newPwmVal >= MAX_PWM_VALUE:
            self.ledDict[led]["pulsatingInc"] = -PULSATING_INCREMENT
            newPwmVal = MAX_PWM_VALUE
        elif newPwmVal <= MIN_PWM_VALUE:
            self.ledDict[led]["pulsatingInc"] = PULSATING_INCREMENT
            newPwmVal = MIN_PWM_VALUE

        if (newPwmVal != currentPwmVal):
            self.ledDict[led]["pwmVal"] = newPwmVal
            self.ledDict[led]["valChanged"] = True

    def PeriodicUpdate(self):
        # Update PWM values going to LEDs based on current state

        while (self.running):
            for led in LedSelection:
                self.mutex.acquire()
                state = self.ledDict[led]["state"]

                if state is LedStates.Off:
                    self.UpdateOffState(led)

                elif state is LedStates.On:
                    self.UpdateOnState(led)

                elif state is LedStates.Toggling:
                    self.UpdateTogglingState(led)

                else:
                    self.UpdatePulsatingState(led)

                self.ChangeDutyCycleIfNeeded(led)
                self.mutex.release()

            time.sleep(UPDATE_PERIOD_S)

######## Below functions are meant to be called from outside of this module ########

    def SetLedState(self, ledToSet, newState):
        if (ledToSet in LedSelection and newState in LedStates):
            self.mutex.acquire()
            self.ledDict[ledToSet]["state"] = newState
            self.ledDict[ledToSet]["pwmVal"] = MIN_PWM_VALUE
            self.ledDict[ledToSet]["valChanged"] = True
            self.mutex.release()

    def SetFrontLedStates(self, newState):
        # Set both front LEDs to new state atomically
        if (newState in LedStates):
            self.mutex.acquire()
            self.ledDict[LedSelection.LeftFrontLed]["state"] = newState
            self.ledDict[LedSelection.LeftFrontLed]["pwmVal"] = MIN_PWM_VALUE
            self.ledDict[LedSelection.LeftFrontLed]["valChanged"] = True
            self.ledDict[LedSelection.RightFrontLed]["state"] = newState
            self.ledDict[LedSelection.RightFrontLed]["pwmVal"] = MIN_PWM_VALUE
            self.ledDict[LedSelection.RightFrontLed]["valChanged"] = True
            PrintToLog("SetFrontLedStates: " + str(newState))
            self.mutex.release()

    def Cleanup(self):
        self.running = False;
        self.periodicUpdateThread.join()

        for led in LedSelection:
            pwmObj = self.ledDict[led]["pwmObj"]
            pwmObj.stop()
