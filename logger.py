import os

thisFileDir = os.path.dirname(os.path.realpath(__file__))
DEBUG_LOG_PATH = thisFileDir + "/debug.log"
ENABLED = True # Can be disabled so that logging doesn't slow down the program

def PrintToLog(strToPrint, init=False):
    if ENABLED:
        # Log file is opened and closed for each write so that we have a log even if our application crashes
        if init:
            mode = "w"
        else:
            mode = "a"

        with open(DEBUG_LOG_PATH, mode) as logFile:
            logFile.write(strToPrint + "\n")
