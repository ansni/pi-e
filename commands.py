# All commands recognized by main control module

import enum

class Commands(enum.Enum):
    SCREEN_MODE = "screen-mode", ["face", "camera"]
    FACE = "face", ["happy", "neutral", "sleepy", "sad"], ["static", "talking"]# 3rd parameter is duration [-1...N] seconds, where -1 means infinite duration
    FACE_BUBBLE_ICON = "nothing", "listening", "thinking", "speech", "question"
    FACE_TOUCHED = "face-touched"
    RECOG_TOUCHED = "recog-touched"
    RECOG_OBJECTS = "recog-objects" # parameter is a list of recognized objects
    RECOG_OBJECT_POS = "recog-obj-pos" # parameter is the recognized object's position
    MOVE = "move" # 1st parameter is speed, 2nd parameter is turn radius
    IMMEDIATE_STOP = "stop"
    SPEAK = "speak" # parameter is text that the robot should speak out
    MIC_SIM = "mic-sim" # parameter is text that user has said to the robot through mic
    RANDOM = "random"
    EXIT = "exit"
    GPIO_SIM_IN = "gpio-sim-in" # 1st parameter is pin, 2nd parameter is pin value
    BTN_LONG_PRESS = "btn-long-press"
    BTN_SHORT_PRESS = "btn-short-press"
    BTN_DOUBLE_PRESS = "btn-double-press"
    HOTWORD = "hotword"

class SpeechCommands(enum.Enum):
    CAMERA = ("kamera", "")
    FACE = ("naama", "")
    FOLLOW = ("seuraa", "seuraan") #the command and response to it
    WAIT = ("odota", "odotan") #the command and response to it
    SHUTDOWN = ("nukkumaan", "hei hei") #the command and response to it

    @classmethod
    def ToString(cls):
        strList = list(map(lambda c: c.value[0], cls))
        str = ""
        for s in strList:
            str = str + ", " + s

        str = str[2:] # remove the first comma and space
        print("SpeechCommands to string: " + str)
        return str
