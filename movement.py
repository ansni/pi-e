# Robot movement based on input commands from main controller

from stub_control import STUB_GPIO, STUB_TIME, STUB_THREADING

if STUB_GPIO:
    import GPIO_stubs as GPIO
else:
    import RPi.GPIO as GPIO

if STUB_TIME:
    from time_stubs import sleep
else:
    from time import sleep

if STUB_THREADING:
    from threading_stubs import Thread, Lock
else:
    from threading import Thread, Lock

from logger import PrintToLog
import gpio_pin_mapping_bcm as Pins
import enum

MIN_POSITIVE_TURN_RADIUS = 1  # cm
MIN_NEGATIVE_TURN_RADIUS = -MIN_POSITIVE_TURN_RADIUS
MAX_POSITIVE_TURN_RADIUS = 100  # cm
MAX_NEGATIVE_TURN_RADIUS = -MAX_POSITIVE_TURN_RADIUS
INFINITE_POSITIVE_TURN_RADIUS = MAX_POSITIVE_TURN_RADIUS + 1
INFINITE_NEGATIVE_TURN_RADIUS = MAX_NEGATIVE_TURN_RADIUS - 1
WHEEL_TO_WHEEL_DIST = 20  # cm
MIN_SPEED = -100  # unitless
MAX_SPEED = 100  # unitless
FW_BW_ACCELERATION = 30  # unitless
UPDATE_PERIOD_S = 0.05

# Precalculated for periodic function CalcInnerMotorSpeed
K1 = float(-MIN_SPEED) / float(WHEEL_TO_WHEEL_DIST)
B1 = float(MIN_SPEED)
K2 = float(MAX_SPEED) / (float(MAX_POSITIVE_TURN_RADIUS) - float(WHEEL_TO_WHEEL_DIST))
B2 = float(-WHEEL_TO_WHEEL_DIST) * float(K2)

class MotorDirPins(enum.Enum):
    Left = (Pins.LEFT_MOTOR_DIR_PIN1, Pins.LEFT_MOTOR_DIR_PIN2)
    Right = (Pins.RIGHT_MOTOR_DIR_PIN1, Pins.RIGHT_MOTOR_DIR_PIN2)

class MotorDirs(enum.Enum):
    Forward = (GPIO.HIGH, GPIO.LOW)
    Backward = (GPIO.LOW, GPIO.HIGH)

class Movement:
    def __init__(self):
        self.running = True
        self.targetPosition = (0, 0)
        self.currentPosition = (0, 0)
        self.targetSpeed = 0
        self.currentSpeed = 0
        self.targetTurnRadius = INFINITE_POSITIVE_TURN_RADIUS
        self.currentTurnRadius = INFINITE_POSITIVE_TURN_RADIUS
        self.leftDir = MotorDirs.Backward
        self.rightDir = MotorDirs.Backward
        self.leftPwmValue = 0
        self.rightPwmValue = 0

        GPIO.setup(Pins.LEFT_MOTOR_PWM_PIN, GPIO.OUT)
        GPIO.setup(Pins.LEFT_MOTOR_DIR_PIN1, GPIO.OUT)
        GPIO.setup(Pins.LEFT_MOTOR_DIR_PIN2, GPIO.OUT)
        GPIO.setup(Pins.RIGHT_MOTOR_PWM_PIN, GPIO.OUT)
        GPIO.setup(Pins.RIGHT_MOTOR_DIR_PIN1, GPIO.OUT)
        GPIO.setup(Pins.RIGHT_MOTOR_DIR_PIN2, GPIO.OUT)
        self.SetMotorDirection(MotorDirPins.Left, self.leftDir, MotorDirs.Forward)
        self.SetMotorDirection(MotorDirPins.Right, self.rightDir, MotorDirs.Forward)
        self.leftPwm = GPIO.PWM(Pins.LEFT_MOTOR_PWM_PIN, 100)
        self.rightPwm = GPIO.PWM(Pins.RIGHT_MOTOR_PWM_PIN, 100)
        self.leftPwm.start(self.leftPwmValue)
        self.rightPwm.start(self.rightPwmValue)

        self.mutex = Lock()
        self.periodicUpdateThread = Thread(target = self.PeriodicUpdate, args = [])
        self.periodicUpdateThread.start()

        PrintToLog("Movement initialized")

    def UpdateCurrentSpeed(self, currentValue, targetValue, stepSize):
        ''' Internal function for movement module '''
        if (currentValue < targetValue):
            currentValue += stepSize
            if (currentValue > targetValue):
                currentValue = targetValue
        elif (currentValue > targetValue):
            currentValue -= stepSize
            if (currentValue < targetValue):
                currentValue = targetValue

        return currentValue

    def CalcInnerMotorSpeed(self, turnRadius, outerMotorSpeed):
        ''' Calculate inner motor speed with formula y = kx + b
        y = inner motor speed [MIN_SPEED, MAX_SPEED],
        x = turnRadius,
        b = min speed with used line,
        k = scaler, different around WHEEL_TO_WHEEL_DIST '''

        if ((turnRadius == INFINITE_NEGATIVE_TURN_RADIUS) or (turnRadius == INFINITE_POSITIVE_TURN_RADIUS)):
            innerMotorSpeedScaled = outerMotorSpeed
        else:
            if abs(turnRadius) < WHEEL_TO_WHEEL_DIST:
                k = K1
                b = B1
                #print("if")
            else:
                k = K2
                b = B2
                #print("else")

            innerMotorSpeed = k * float(abs(turnRadius)) + b

            # Scale inner motor speed down based on outerMotorSpeed and add +- marker
            downScaler = float(outerMotorSpeed) / float(MAX_SPEED)
            innerMotorSpeedScaled = innerMotorSpeed * downScaler

            #print("k = " + str(k) + ", b = " + str(b) + ", ds = " + str(downScaler) + ", i = " + str(innerMotorSpeed) + ", is = " + str(innerMotorSpeedScaled) + ", o = " + str(outerMotorSpeed) + ", tr = " + str(turnRadius))

        return int(innerMotorSpeedScaled)

    def SetMotorDirection(self, dirPins, currentDirPinValues, newDirPinValues):
        ''' Internal function for movement module '''
        if (currentDirPinValues != newDirPinValues):
           GPIO.output(dirPins.value[0], newDirPinValues.value[0])
           GPIO.output(dirPins.value[1], newDirPinValues.value[1])
           currentDirPinValues = newDirPinValues

        return currentDirPinValues

    def ChangeDutyCycleIfNeeded(self, pwmObject, currentSpeed, newSpeed):
        ''' Internal function for movement module '''
        if (currentSpeed != newSpeed):
            currentSpeed = newSpeed
            pwmObject.ChangeDutyCycle(newSpeed)

        return currentSpeed

    def PeriodicUpdate(self):
        # Update PWM values going to motors
        # Infinite loop is outside 'Update' function so that one can be unit tested
        while (self.running):
            self.Update()

    def Update(self):
        self.mutex.acquire()
        targetSpeed = self.targetSpeed
        self.currentTurnRadius = self.targetTurnRadius # no acceleration for turn radius, we apply it directly
        self.mutex.release()

        self.currentSpeed = self.UpdateCurrentSpeed(
            self.currentSpeed, targetSpeed, FW_BW_ACCELERATION)

        #print("currentSpeed: " + str(self.currentSpeed) + ", currentTurnRadius : " + str(self.currentTurnRadius))

        if (self.currentTurnRadius >= 0):
            # turning right
            leftMotorSpeed = self.currentSpeed
            rightMotorSpeed = self.CalcInnerMotorSpeed(
                self.currentTurnRadius, self.currentSpeed)

        else:
            # turning left
            rightMotorSpeed = self.currentSpeed
            leftMotorSpeed = self.CalcInnerMotorSpeed(
                self.currentTurnRadius, self.currentSpeed)

        if (leftMotorSpeed >= 0):
            dir = MotorDirs.Forward
        else:
            dir = MotorDirs.Backward

        self.leftDir = self.SetMotorDirection(MotorDirPins.Left, self.leftDir, dir)

        if (rightMotorSpeed >= 0):
            dir = MotorDirs.Forward
        else:
            dir = MotorDirs.Backward

        self.rightDir = self.SetMotorDirection(MotorDirPins.Right, self.rightDir, dir)

        #print("setting left pwm: " + str(leftMotorSpeed))
        self.leftPwmValue = self.ChangeDutyCycleIfNeeded(
            self.leftPwm, self.leftPwmValue, abs(leftMotorSpeed))

        #print("setting right pwm: " + str(rightMotorSpeed))
        self.rightPwmValue = self.ChangeDutyCycleIfNeeded(
            self.rightPwm, self.rightPwmValue, abs(rightMotorSpeed))

        sleep(UPDATE_PERIOD_S)

######## Below functions are meant to be called from outside of this module ########

    def SetSpeed(self, speed):
        ''' speed = MIN_SPEED...MAX_SPEED
                Positive values for going forward, negative values for going backwards.
        '''
        if speed < MIN_SPEED:
            speed = MIN_SPEED
        elif speed > MAX_SPEED:
            speed = MAX_SPEED

        # store the new value as target value. Current value will reach it eventually
        self.mutex.acquire()
        self.targetSpeed = speed
        self.mutex.release()
        #print("SetSpeed: " + str(speed))

    def SetTurnRadius(self, turnRadius):
        ''' turnRadius = MIN_TURN_RADIUS...MAX_TURN_RADIUS.
                Positive values for turning right, negative values for turning left.
                Turn radiusese smaller or bigger than limit radiuses are interpreted as infinite radius (straight line)
        '''

        if turnRadius < MAX_NEGATIVE_TURN_RADIUS:
            turnRadius = INFINITE_NEGATIVE_TURN_RADIUS
        elif turnRadius > MAX_POSITIVE_TURN_RADIUS:
            turnRadius = INFINITE_POSITIVE_TURN_RADIUS

        # zero radius is not allowed as turn direction cannot be deduced from it
        if turnRadius != 0:
            # store the new value as target value. Current value will reach it eventually
            self.mutex.acquire()
            self.targetTurnRadius = turnRadius
            self.mutex.release()
            #print("SetTurnRadius: " + str(turnRadius))

    def Stop(self):
        ''' stop immediately without deceleration '''
        self.mutex.acquire()
        self.targetSpeed = 0
        self.currentSpeed = 0
        self.mutex.release()

    def Cleanup(self):
        self.running = False;
        self.periodicUpdateThread.join()
        self.leftPwm.stop()
        self.rightPwm.stop()
