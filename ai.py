# AI for self started actions

import os
import enum
import time
from random import shuffle
from threading import Thread
import queue
from client_socket import ClientSocket
from ip_settings import LOCAL_IP, PORT
from logger import PrintToLog
from wit import Wit
from commands import Commands
from movement import MAX_SPEED, INFINITE_POSITIVE_TURN_RADIUS
from ai_responder import AIResponder

WIT_ACCESS_TOKEN_FILE = "wit_access_token.txt"
WIT_ACCESS_TOKEN_LEN = 32
THIS_FILE_DIR = os.path.dirname(os.path.realpath(__file__))
UPDATE_PERIOD_S = 0.5
FOLLOW_CONTROL_P_COEFF = 0.15 # can be tuned
RANDOM_RESPONSE_PERIOD_S = 30
FACE_EXPRESSION_PERIOD_S = 10
FACE_EXPRESSION_DURATION_S = "3"

class AIModes(enum.Enum):
    Stationary = 1
    Follow = 2

class QItemTypes(enum.Enum):
    AIMode = 1
    UpdateFollowedObjPos = 2
    RespToStr = 3
    RespToFaceTouch = 4
    RespToRecogTouch = 5
    RespToObjectRecog = 6
    RespRandomly = 7

class QItem:
    def __init__(self, type, data, Callback):
        self.type = type
        self.data = data
        self.Callback = Callback

class AI:
    def __init__(self):
        wit_access_token = ""
        with open(THIS_FILE_DIR + "/" + WIT_ACCESS_TOKEN_FILE) as f:
            wit_access_token = f.read(WIT_ACCESS_TOKEN_LEN)

        self.aiResponder = AIResponder()
        self.witClient = Wit(wit_access_token)
        self.socket = ClientSocket(LOCAL_IP, PORT)
        self.mode = AIModes.Stationary
        self.followedObjectPos = (-1, -1)
        self.followingSeenObject = False
        self.faceExpressionTimestamp = time.time()
        self.randRespTimestamp = time.time()
        self.running = True
        self.reqQueue = queue.Queue()
        self.perQueue = queue.Queue()
        self.workerThreadHandleRequests = Thread(target = self.WorkerHandleRequests, args = [])
        self.workerThreadHandleRequests.start()
        self.workerThreadPeriodicActions = Thread(target = self.WorkerPeriodicActions, args = [])
        self.workerThreadPeriodicActions.start()

        self.socket.SendToServer(Commands.SPEAK.value + "#Heippa!")
        PrintToLog("AI initialized")

    def WorkerPeriodicActions(self):

        while True:

            # Decide if something spontaneous should be started.
            # Send actions to main controller through socket so that the
            # main controller can make sure no conflicting operations are ongoing

            # Check for requests from queue but don't wait for them
            try:
                item = self.perQueue.get_nowait()

                if item is None:
                    break #exit thread

                if item.type == QItemTypes.AIMode:
                    self.mode = item.data
                elif item.type == QItemTypes.UpdateFollowedObjPos:
                    self.followedObjectPos = item.data

            except queue.Empty:
                pass

            # Common actions for all modes
            self.PeriodicCommonActions()

            if (self.mode == AIModes.Stationary):
                # Stay still
                self.PeriodicStationaryActions()

            else:
                # Follow seen object
                self.PeriodicFollowActions()

            time.sleep(UPDATE_PERIOD_S)

    def PeriodicCommonActions(self):
        currentTime = time.time()
        if (currentTime - self.randRespTimestamp > RANDOM_RESPONSE_PERIOD_S):
            self.randRespTimestamp = currentTime
            # commented out for now until conversation is debugged to work smoothly
            #self.ResponseRandomly(self.RandomResponseReady)

    def RandomResponseReady(self, respStr):
        self.socket.SendToServer(Commands.SPEAK.value + "#" + respStr)

    def PeriodicStationaryActions(self):
        # Face is shown only in stationary AI state
        # Update face expression once in a while
        currentTime = time.time()
        if (currentTime - self.faceExpressionTimestamp > FACE_EXPRESSION_PERIOD_S):
            self.faceExpressionTimestamp = currentTime

            # Choose a non-sad emotion randomly and static action
            emotions = Commands.FACE.value[1]
            shuffle(emotions)
            actions = Commands.FACE.value[2]
            self.socket.SendToServer(Commands.FACE.value[0] + "#" + emotions[0] + "#" + actions[0] + "#" + FACE_EXPRESSION_DURATION_S)

    def PeriodicFollowActions(self):
        # Follow mode where the AI tries to move after certain object
        posX = self.followedObjectPos[0]
        if (posX > -1):
            # if x position is on the left side of screen, turn left. Otherwise turn right.
            # P(ID) controller is used to do this smoothly
            PrintToLog("AI followed seen object. Pos x: " + str(posX))
            targetPosX = 50 #middle of screen (range is from 0 to 100)
            diff = targetPosX - posX
            controlVal = FOLLOW_CONTROL_P_COEFF * diff

            # positive control value means turn left, negative right
            speed = MAX_SPEED
            if controlVal != 0:
                turnRadius = int(-100/controlVal)
            else:
                turnRadius = INFINITE_POSITIVE_TURN_RADIUS

            self.followingSeenObject = True
            PrintToLog("Sending turn radius: " + str(turnRadius) + ", and speed: " + str(speed))
            self.socket.SendToServer(Commands.MOVE.value + "#" + str(speed) + "#" + str(turnRadius))

        elif (self.followingSeenObject):
            # Stop until followed object is seen again
            self.followingSeenObject = False
            self.socket.SendToServer(Commands.MOVE.value + "#0#0")

    def WorkerHandleRequests(self):

        while True:

            # Wait for requests from queue
            item = self.reqQueue.get()
            if item is None:
                break #exit thread

            # prevent random response to occur too close to normal response
            currentTime = time.time()
            self.randRespTimestamp = currentTime

            # prevent spontanious facial expression to occur too close to talking expression
            self.faceExpressionTimestamp = currentTime

            response = ""
            if item.type == QItemTypes.RespToStr:
                response = self.RespToStr(item.data)

            elif item.type == QItemTypes.RespToFaceTouch:
                response = self.RespToFaceTouch()

            elif item.type == QItemTypes.RespToRecogTouch:
                response = self.RespToRecogTouch()

            elif item.type == QItemTypes.RespToObjectRecog:
                response = self.RespToObjectRecog(item.data)

            elif item.type == QItemTypes.RespRandomly:
                response = self.RespRandomly()

            # Response to the requester
            if response != "":
                item.Callback(response)

            time.sleep(UPDATE_PERIOD_S) # Prevent CPU exhaustion if there are lots of requests

    def WitAnalyse(self, inputStr):
        PrintToLog("User input to AI: " + inputStr)
        response = ""
        try:
            resp = self.witClient.message(inputStr)
            PrintToLog("Wit response: " + str(resp))
            response = self.aiResponder.HandleUserMessage(resp)
        except:
            response = "Anteeksi en ymmärrä"

        PrintToLog("AI response to user: " + response)
        return response

    def RespToStr(self, inputStr):
        response = self.WitAnalyse(inputStr)
        return response

    def RespToObjectRecog(self, recogNamesList):
        text = "Kuvassa on "
        count = 0
        for name in recogNamesList:
            count += 1
            text += name
            if count == len(recogNamesList):
                pass # no dot at the end needed
            elif count == len(recogNamesList) - 1:
                text += " ja "
            else:
                text += ", "

        response = self.WitAnalyse(text)
        return response

    def RespToFaceTouch(self):
        response = self.WitAnalyse("Koskin sinuun")
        return response

    def RespToRecogTouch(self):
        response = self.WitAnalyse("Koskin kamerakuvaan")
        return response

    def RespRandomly(self):
        response = self.WitAnalyse("Sano jotain satunnaista")
        return response

######## Functions that can be called outside this module ########

    def SetToMode(self, aiMode):
        if aiMode in AIModes:
            item = QItem(QItemTypes.AIMode, aiMode, None)
            self.perQueue.put(item)

    def UpdateFollowedObjectPosition(self, posTuple):
        item = QItem(QItemTypes.UpdateFollowedObjPos, posTuple, None)
        self.perQueue.put(item)

    def ResponseTo(self, inputStr, Callback):
        item = QItem(QItemTypes.RespToStr, inputStr, Callback)
        self.reqQueue.put(item)

    def ResponseToFaceTouch(self, Callback):
        item = QItem(QItemTypes.RespToFaceTouch, "", Callback)
        self.reqQueue.put(item)

    def ResponseToRecogTouch(self, Callback):
        item = QItem(QItemTypes.RespToRecogTouch, "", Callback)
        self.reqQueue.put(item)

    def ResponseToObjectRecog(self, names, Callback):
        item = QItem(QItemTypes.RespToObjectRecog, names, Callback)
        self.reqQueue.put(item)

    def ResponseRandomly(self, Callback):
        item = QItem(QItemTypes.RespRandomly, "", Callback)
        self.reqQueue.put(item)

    def Cleanup(self):
        self.reqQueue.put(None)
        self.workerThreadHandleRequests.join()
        self.perQueue.put(None)
        self.workerThreadPeriodicActions.join()
        self.socket.Cleanup()
        self.aiResponder.Cleanup()
        PrintToLog("AI cleanup")
