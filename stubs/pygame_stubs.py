# Stubs for Pygame display and event functions

class pygame:

    MOUSEBUTTONDOWN = 0
    MOUSBUTTONUP = 1

    HWSURFACE = 0
    DOUBLEBUF = 1

    class display:
        def init():
            pass

        def set_mode(size, flags):
            return None

        def update():
            pass

    class mouse:
        def set_visible(isVisible):
            pass

    class event:

        type = 0

        def __init__(self):
            pass

        def __iter__(self):
            return pygame.event.iterator()

        def get():
            return pygame.event()

        class iterator:
            def __iter__(self):
                return self

            def __next__(self):
                raise StopIteration

    class sprite:
        class Sprite:
            def __init__(self):
                pass

            def draw():
                pass

        class Group:
            def __init__(self, spriteObj):
                pass

            def draw(self, screen):
                pass

    class Rect:
        def __init__(self, x, y, width, height):
            pass

    class image:
        def load(path):
            pass

    def quit():
        pass

