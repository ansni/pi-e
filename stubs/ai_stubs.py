# Stubbed AI module

from time import sleep
from threading import Thread

class AIModes:
    Stationary = 1
    Follow = 2

class AI:
    def __init__(self):
        pass

    def ResponseTo(self, inputStr, CallbackFunc):
        thinkingThread = Thread(target = self.ResponseThinkingThread, args = [inputStr, CallbackFunc])
        thinkingThread.start()        

    def ResponseThinkingThread(self, inputStr, CallbackFunc):
        sleep(3)
        response = "Response from AI stub"
        CallbackFunc(response)

    def Cleanup(self):
        pass