# Sound output module stubbed

from time import sleep
from threading import Thread
from logger import PrintToLog

class SoundOut:
    def __init__(self):
        pass

    def Play(self, soundFilePath, FinishedCallback):
        playThread = Thread(target = self.PlayThread, args = [soundFilePath, FinishedCallback])
        playThread.start() 

    def PlayThread(self, soundFilePath, FinishedCallback):
        sleep(5)
        PrintToLog("Sound played in stubbed function")
        FinishedCallback()

    def Cleanup(self):
        pass
