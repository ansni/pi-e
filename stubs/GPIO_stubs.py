HIGH = 1
LOW = 0

IN = 0
OUT = 1

PUD_DOWN = 0
PUD_UP = 1

BCM = 0

SIMULATED_INPUT_VALUE = {"1":1,
                         "2":1,
                         "3":1,
                         "4":1,
                         "5":1}

def setup(pin, direction, pull_up_down=0):
    print("GPIO setup called")

def setmode(mode):
    print("GPIO setmode called")

def output(pin, value):
    print("GPIO output called. Pin: " + str(pin) + ", value: " + str(value))

def input(pin):
    pinStr = str(pin)
    if pinStr in SIMULATED_INPUT_VALUE.keys():
        value = SIMULATED_INPUT_VALUE[pinStr]
    else:
        value = 0

    return value

def cleanup():
    print("GPIO cleanup called.")

class PWM:
    def __init__(self, pin, pwmMaxValue):
        self.pin = pin
        print("GPIO PWM created. Pin: " + str(pin))

    def start(self, duty):
        print("GPIO start called")

    def ChangeDutyCycle(self, duty):
        print("GPIO ChangeDutyCycle called. Pin: " +
              str(self.pin) + ", Duty: " + str(duty))

    def stop(self):
        print("GPIO stop called")
