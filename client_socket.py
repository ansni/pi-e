# Import socket module
import socket


class ClientSocket:
    '''Helper class to use create and use a client socket'''

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP socket

    def SendToServer(self, strData):
        self.s.sendto(strData.encode("utf-8"), (self.ip, self.port))

    def Cleanup(self):
        # close the socket
        self.s.close()
