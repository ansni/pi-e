# Converts text to voice file
from stub_control import STUB_GOOGLE_CLOUD
if STUB_GOOGLE_CLOUD:
    from google_cloud_stubs import texttospeech
else:
    from google.cloud import texttospeech

from threading import Thread
from queue import Queue
from time import sleep
from logger import PrintToLog
import os

THIS_FILE_DIR = os.path.dirname(os.path.realpath(__file__))
SOUND_FILE_PATH = THIS_FILE_DIR + "/sound_to_play.mp3"

UPDATE_PERIOD_S = 0.5

class QItem:
    def __init__(self, text, Callback):
        self.text = text
        self.Callback = Callback

class TextToSpeech:
    def __init__(self, languageCode):

        # Authenticate with Google Cloud account
        if not "GOOGLE_APPLICATION_CREDENTIALS" in os.environ:
            os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = THIS_FILE_DIR + "/google_access_token.json"

        self.client = texttospeech.TextToSpeechClient()
        self.audioConfig = texttospeech.types.AudioConfig(audio_encoding=texttospeech.enums.AudioEncoding.MP3)
        self.voice = texttospeech.types.VoiceSelectionParams(language_code=languageCode, ssml_gender=texttospeech.enums.SsmlVoiceGender.NEUTRAL)

        self.queue = Queue()
        self.conversionThread = Thread(target = self.ConversionWorker, args = [])
        self.conversionThread.start()

        PrintToLog("Text-to-speech initialized")

    def StartConversion(self, textStr, ConversionReadyCallbackFunc):
        item = QItem(textStr, ConversionReadyCallbackFunc)
        self.queue.put(item)

    def ConversionWorker(self):

        while True:

            item = self.queue.get()
            if item is None:
                break # exit thread

            synthesisInput = texttospeech.types.SynthesisInput(text=item.text)
            response = self.client.synthesize_speech(synthesisInput, self.voice, self.audioConfig)
            # The response's audio_content is binary.
            with open(SOUND_FILE_PATH, "wb") as out:
                # Write the response to the output file.
                out.write(response.audio_content)

            PrintToLog("Text to speech audio content written to file: " + SOUND_FILE_PATH)
            item.Callback(SOUND_FILE_PATH)

            sleep(UPDATE_PERIOD_S) #prevent CPU exhaustion if queue has lots of items

    def Cleanup(self):
        self.queue.put(None)        
        self.conversionThread.join()
