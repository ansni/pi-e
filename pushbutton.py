# Pushbutton that has two actions: short push or long push

from stub_control import STUB_GPIO

if STUB_GPIO:
    import GPIO_stubs as GPIO
else:
    import RPi.GPIO as GPIO

from threading import Thread
from client_socket import ClientSocket
from ip_settings import LOCAL_IP, PORT
from logger import PrintToLog
from commands import Commands
import gpio_pin_mapping_bcm as Pins
import time

UPDATE_PERIOD_S = 0.05
PIN_ACTIVE = GPIO.LOW
PIN_INACTIVE = GPIO.HIGH
LONG_PRESS_TIME_S = 3
DOUBLE_PRESS_GAP_S = 1

class Pushbutton:
    def __init__(self):
        GPIO.setup(Pins.PUSH_BTN_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        self.socket = ClientSocket(LOCAL_IP, PORT)
        self.running = True
        self.btnPrevState = PIN_INACTIVE
        self.btnActiveStartTime = 0
        self.btnDeactStartTime = 0
        self.btnReleaseCount = 0
        self.btnPressSent = False
        self.btnReleaseSent = True
        self.btnPollingThread = Thread(target = self.BtnPollingWorker, args = [])
        self.btnPollingThread.start()

        PrintToLog("Pushbutton module initialized")

    def BtnPollingWorker(self):
        # Checks if pushbutton is pressed and sends command to main controller if so
        while self.running:

            currentTime = time.time()
            if (GPIO.input(Pins.PUSH_BTN_PIN) == PIN_ACTIVE):
                if (self.btnPrevState == PIN_ACTIVE):
                    if ((not self.btnPressSent) and (currentTime - self.btnActiveStartTime > LONG_PRESS_TIME_S)):
                        self.socket.SendToServer(Commands.BTN_LONG_PRESS.value)
                        self.btnPressSent = True # Prevents multiple sends when button is kept pressed
                else:
                    self.btnActiveStartTime = currentTime
                    self.btnPressSent = False

                self.btnPrevState = PIN_ACTIVE

            else:
                if (not self.btnPressSent):
                    # Send command at button release only if command was not sent at press
                    if (self.btnPrevState == PIN_INACTIVE):
                        if (not self.btnReleaseSent):
                            if (self.btnReleaseCount == 2):
                                self.socket.SendToServer(Commands.BTN_DOUBLE_PRESS.value)
                                self.btnReleaseCount = 0
                                self.btnReleaseSent = True

                            elif (currentTime - self.btnDeactStartTime > DOUBLE_PRESS_GAP_S):
                                self.socket.SendToServer(Commands.BTN_SHORT_PRESS.value)
                                self.btnReleaseCount = 0
                                self.btnReleaseSent = True

                    else:
                        self.btnReleaseCount = self.btnReleaseCount + 1
                        self.btnReleaseSent = False
                        self.btnDeactStartTime = currentTime

                self.btnPrevState = PIN_INACTIVE

            time.sleep(UPDATE_PERIOD_S)

    def Cleanup(self):
        self.running = False;
        self.btnPollingThread.join()
        self.socket.Cleanup()
        PrintToLog("Pushbutton cleanup")
