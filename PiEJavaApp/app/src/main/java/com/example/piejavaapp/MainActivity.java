package com.example.piejavaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements JoystickView.JoystickListener{

    private long prevSendTimeMs;
    private long sendDelayMs = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JoystickView joystick = new JoystickView(this);
        setContentView(R.layout.activity_main);
        this.prevSendTimeMs = 0;
        this.sendDelayMs = 10;

        final EditText editText = (EditText) findViewById(R.id.words_to_send);
        editText.setHorizontallyScrolling(false);
        editText.setLines(3);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    String text = editText.getText().toString();
                    String cmdStr = "speak#" + text;
                    sendMessage(cmdStr);
                    editText.setText("");
                    handled = true;
                }
                return handled;
            }
        });

        final Button surpriseBtn = (Button) findViewById(R.id.surprise_btn);
        surpriseBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String cmdStr = "random";
                sendMessage(cmdStr);
            }
        });
    }

    @Override
    public void onJoystickMoved(float xPercent, float yPercent, int id) {

        // Send movement info forward only every x ms unless it's the last movement which is joystick in center position
        long currentTimeMs = System.currentTimeMillis();
        if ((currentTimeMs - this.prevSendTimeMs > this.sendDelayMs) || (xPercent == 0 && yPercent == 0))
        {
            this.prevSendTimeMs = currentTimeMs;
            Pair<Integer, Integer> speedAndTurnRadius = convertToSpeedAndTurnRadius(xPercent, yPercent);
            switch (id)
            {
                case R.id.joystick:
                    sendMessage("move#" + speedAndTurnRadius.first.toString() + "#" + speedAndTurnRadius.second.toString());
                    break;
            }
        }
    }

    private Pair<Integer, Integer> convertToSpeedAndTurnRadius(float xPercent, float yPercent) {
        // xPercent goes from -1.0 to 1.0 horizontally from left to right
        // yPercent goes from -1.0 to 1.0 vertically from top to bottom
        // Those values need to be converted to speed and turn radius
        // Speed is the distance from center position. Upper half circle is positive speed and lower half circle is negative speed. Range [-100,100]
        // Turn radius goes from 1 to 101 or -1 to -101 where 101 and -101 mean straight line. Positive radius means turning right and negative radius means turning left.
        Integer absSpeed = (int)Math.sqrt(Math.pow((int)(xPercent * 100.0), 2) + Math.pow((int)(yPercent * 100.0), 2));
        Integer speedSign = 1;
        if (yPercent > 0) {
            speedSign = -1;
        }
        Integer speed = absSpeed * speedSign;

        // Angle from horizontal center to vertical center line determines turn radius.
        // Angle between 0 and 1 deg equals to turn radius 1.
        // Angle equal or greater to 90 deg equals to turn radius 101.
		// Special case is when speed is 0. Then turn radius does not have meaningful value.
        Integer turnRadius = 0;
		if (speed != 0) {
			double angleDeg = Math.toDegrees(Math.atan2(Math.abs(yPercent), Math.abs(xPercent)));

			turnRadius = (int)(angleDeg / 0.9) + 1;
			if (turnRadius > 101) {
				turnRadius = 101;
			} else if (turnRadius < 1) {
				turnRadius = 1;
			}
			if (xPercent < 0) {
				turnRadius *= -1; // set sign
			}
		}

        Pair<Integer, Integer> speedAndTurnRadius = Pair.create(speed, turnRadius);
        return speedAndTurnRadius;
    }

    private void sendMessage(final String msg) {
        UDPClient Client = new UDPClient();
        Client.Message = msg;
        Client.Send();
    }
}
