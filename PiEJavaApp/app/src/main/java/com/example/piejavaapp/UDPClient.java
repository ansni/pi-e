package com.example.piejavaapp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;

public class UDPClient
{
    private InetAddress IPAddress = null;
    private AsyncTask<Void, Void, Void> asyncClient;
    public String Message;

    @SuppressLint("NewApi")
    public void Send()
    {
        this.asyncClient = new AsyncTask<Void, Void, Void>()
        {
            @Override
            protected Void doInBackground(Void... params)
            {
                DatagramSocket ds = null;

                try
                {
                    byte[] ipAddr = new byte[]{ (byte) 192, (byte) 168, (byte) 1, (byte) 106};
                    InetAddress addr = InetAddress.getByAddress(ipAddr);
                    ds = new DatagramSocket();
                    DatagramPacket dp;
                    dp = new DatagramPacket(Message.getBytes(), Message.getBytes().length, addr, 5005);
                    ds.send(dp);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    if (ds != null)
                    {
                        ds.close();
                    }
                }
                return null;
            }

            protected void onPostExecute(Void result)
            {
                super.onPostExecute(result);
            }
        };

        asyncClient.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}