STUB_GPIO = False
STUB_TIME = False
STUB_THREADING = False
STUB_GOOGLE_CLOUD = False
STUB_FACE_RECOGNITION = False
STUB_PYGAME = False
STUB_AI = False
STUB_SOUND_OUT = False

def SetupStubDir():
    import os
    import sys

    # Following lines are for assigning stubs directory dynamically.
    dirPath = os.path.dirname(os.path.realpath(__file__))
    stubsDirPath = os.path.abspath(os.path.join(dirPath, "stubs"))
    sys.path.insert(0, stubsDirPath)
