# AI responses to input text. Based on entities returned by Wit.ai.

from random import shuffle
from logger import PrintToLog
import datetime

DATE_TIMES = {
    'today': [
        'Tänään on ' + str(datetime.date.today()),
    ],
    'tomorrow': [
        'Huomenna on ' + str(datetime.date.today() + datetime.timedelta(days=1))
    ],
    'yesterday': [
        'Eilen oli ' + str(datetime.date.today() - datetime.timedelta(days=1))
    ],
    'default': [
        'Tämä hetki on tärkein'
    ],
}

NAMES = {
    'own': [
        'Vainu',
        'Rolle',
        'Rekku',
        'Kaja',
        'Samppa',
        'Toma'
    ],
    'user': [
        'Se on kiva nimi'
    ],
    'default': [
        'Kuka?'
    ],
}

POSITIVE_GREETINGS = {
    'start_conversation': [
        'Heippa',
        'Terve',
        'Moi',
        'Hei',
        'Hyvää päivää',
    ],
    'end_conversation': [
        'Nähdään taas',
        'Hei hei',
        'Heippa'
    ],
    'default': [
        'Hei',
    ]
}

NEGATIVE_GREETINGS = {
    'start_conversation': [
        'Ei heippoja sinulle',
        'Huonoa päivää',
        'Jahas',
    ],
    'end_conversation': [
        'Ei heippoja sinulle',
        'Höh',
        'Jahas',
    ],
    'default': [
        'Hei',
    ]
}

JOKING_GREETINGS = {
    'start_conversation': [
        'Heippa löllerö',
        'Mitä kuuluu kukkuluuruu',
        'Terve tötterö',
    ],
    'end_conversation': [
        'Hei hei löllerö',
        'Moikka makkara',
    ],
    'default': [
        'Hei',
    ]
}

TOUCH_RESPONSES = {
    'face': [
        'hihhih, kutittaa',
        'tunsin tuon',
        'hei eipä tökitä',
        'kenen sormi se oli',
    ],
    'camera': [
        'näkyykö tuttuja?',
        'keitä tunnistat?',
        'löydätkö kuvasta itsesi?',
        'oletko se sinä Fransuaa?'
    ],
    'default': [
        'hihhih, kutittaa',
    ]
}

CAMERA_RECOG_RESPONSES = {
    'human': [
        'oletko se sinä Fransuaa?',
        'hei minähän tunnistan sinut!',
        'tuttuja näköpiirissä',
        'tuttuja naamoja',
        'saisiko olla kahvia?',
        'saisinko lisää sähköä?',
        'terve',
        'kuka sin olet?',
    ],
    'default': [
        'etsin ihmisiä',
    ]
}

RANDOM_RESPONSES = {
    'positive': [
        'heippa',
        'olen kaunis kukka',
        'onkohan kuu juustoa',
        'Kiinnostavaa',
        'Kerro lisää',
        'Kiva kuulla'
    ],
    'negative': [
        'ökkäpökkä',
        'tämä haiskahtaa',
        'Jahas',
        'Höh',
        'Pläät'
    ],
    'default': [
        'öö',
        'aijaa'
    ]
}

class AIResponder:

    def __init__(self):
        self.prevResponse = ''
        PrintToLog('AIResponder initialized')

    def GetEntityRole(self, entities, entity):
        role = None
        for e in entities:
            if entity in e:
                # entity key returned by wit contains also role after : character
                role = e.split(':')[1]
                PrintToLog('Found role \"' + role + '\" for entity \"' + entity + '\"')
                break

        return role

    def RespToGetDate(self, dateTimeRole):
        respDates = DATE_TIMES[dateTimeRole or 'default']
        shuffle(respDates)
        return respDates[0]

    def RespToGetName(self, nameRole):
        respNames = NAMES[nameRole or 'default']
        shuffle(respNames)
        return respNames[0]

    def RespToGreeting(self, greetingRole, sentimentRole):
        if (sentimentRole == 'negative'):
            respGreetings = NEGATIVE_GREETINGS[greetingRole or 'default']
        elif (sentimentRole == 'joking'):
            respGreetings = JOKING_GREETINGS[greetingRole or 'default']
        else: #positive sentiment
            respGreetings = POSITIVE_GREETINGS[greetingRole or 'default']

        shuffle(respGreetings)
        return respGreetings[0]

    def RespToTouching(self, touchedRole):
        respTouches = TOUCH_RESPONSES[touchedRole or 'default']
        shuffle(respTouches)
        return respTouches[0]

    def RespToCameraRecognition(self, recogRole):
        respRecog = CAMERA_RECOG_RESPONSES[recogRole or 'default']
        shuffle(respRecog)
        return respRecog[0]

    def RespToRandom(self, sentimentRole):
        respRands = RANDOM_RESPONSES[sentimentRole or 'default']
        shuffle(respRands)
        return respRands[0]

######## Below are interface functions which are to be called from outside of this module ########

    def HandleUserMessage(self, message):
        entities = message['entities']
        clarifRole = self.GetEntityRole(entities, 'clarification')
        dateTimeRole = self.GetEntityRole(entities, 'date_time')
        respExpectedRole = self.GetEntityRole(entities, 'response_expected')
        nameRole = self.GetEntityRole(entities, 'name')
        sentimentRole = self.GetEntityRole(entities, 'sentiment')
        greetingRole = self.GetEntityRole(entities, 'greeting')
        touchedRole = self.GetEntityRole(entities, 'touched')
        recogRole = self.GetEntityRole(entities, 'camera_recog')
        randomRole = self.GetEntityRole(entities, 'random')

        resp = ''
        if clarifRole :
            resp = self.prevResponse
        elif dateTimeRole:
            resp = self.RespToGetDate(dateTimeRole)
        elif nameRole:
            resp = self.RespToGetName(nameRole)
        elif greetingRole:
            resp = self.RespToGreeting(greetingRole, sentimentRole)
        elif touchedRole:
            resp = self.RespToTouching(touchedRole)
        elif recogRole:
            resp = self.RespToCameraRecognition(recogRole)
        else:
            resp = self.RespToRandom(sentimentRole)

        self.prevResponse = resp
        return resp

    def Cleanup(self):
        PrintToLog("AIResponder cleanup")
