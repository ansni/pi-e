# Test program to send commands to server socket

#!/usr/bin/env python3

import os
import sys

# Following lines are for assigning parent directory dynamically.
dirPath = os.path.dirname(os.path.realpath(__file__))
parentDirPath = os.path.abspath(os.path.join(dirPath, os.pardir))
sys.path.insert(0, parentDirPath)

from client_socket import ClientSocket
from ip_settings import LOCAL_IP, PORT

s = ClientSocket(LOCAL_IP, PORT)
usrCmd = ""
while usrCmd != "q":
    usrCmd = str(input("give cmd (q for quit): "))
    if usrCmd != "q":
        s.SendToServer(usrCmd)

s.Cleanup()
