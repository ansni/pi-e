# Unit tests for movement module

#!/usr/bin/env python3

import os
import sys

# Following lines are for assigning parent directory dynamically.
dirPath = os.path.dirname(os.path.realpath(__file__))
parentDirPath = os.path.abspath(os.path.join(dirPath, os.pardir))
sys.path.insert(0, parentDirPath)

import stub_control
stub_control.STUB_GPIO = True
stub_control.STUB_TIME = True
stub_control.STUB_THREADING = True
stub_control.STUB_GOOGLE_CLOUD = True
stub_control.SetupStubDir()

from movement import *

def testUpdateBeforeInput():
    print(">>>> testUpdateBeforeInput >>>>")
    move = Movement()
    move.Update()

    assert move.targetSpeed == 0
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == INFINITE_POSITIVE_TURN_RADIUS
    assert move.currentTurnRadius == INFINITE_POSITIVE_TURN_RADIUS
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testSetSpeed_max():
    print(">>>> testSetSpeed_max >>>>")
    move = Movement()
    move.SetSpeed(MAX_SPEED)
    assert move.targetSpeed == MAX_SPEED
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == INFINITE_POSITIVE_TURN_RADIUS
    assert move.currentTurnRadius == INFINITE_POSITIVE_TURN_RADIUS
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testSetSpeed_maxPlusOne():
    print(">>>> testSetSpeed_maxPlusOne >>>>")
    move = Movement()
    move.SetSpeed(MAX_SPEED + 1)
    assert move.targetSpeed == MAX_SPEED


def testSetSpeed_min():
    print(">>>> testSetSpeed_min >>>>")
    move = Movement()
    move.SetSpeed(MIN_SPEED)
    assert move.targetSpeed == MIN_SPEED
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == INFINITE_POSITIVE_TURN_RADIUS
    assert move.currentTurnRadius == INFINITE_POSITIVE_TURN_RADIUS
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testSetSpeed_minMinusOne():
    print(">>>> testSetSpeed_minMinusOne >>>>")
    move = Movement()
    move.SetSpeed(MIN_SPEED - 1)
    assert move.targetSpeed == MIN_SPEED


def testSetTurnRadius_positiveMax():
    print(">>>> testSetTurnRadius_positiveMax >>>>")
    move = Movement()
    move.SetTurnRadius(MAX_POSITIVE_TURN_RADIUS)
    assert move.targetSpeed == 0
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == MAX_POSITIVE_TURN_RADIUS
    assert move.currentTurnRadius == INFINITE_POSITIVE_TURN_RADIUS
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testSetTurnRadius_negativeMax():
    print(">>>> testSetTurnRadius_negativeMax >>>>")
    move = Movement()
    move.SetTurnRadius(MAX_NEGATIVE_TURN_RADIUS)
    assert move.targetSpeed == 0
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == MAX_NEGATIVE_TURN_RADIUS
    assert move.currentTurnRadius == INFINITE_POSITIVE_TURN_RADIUS
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testSetTurnRadius_positiveMaxPlusOne():
    print(">>>> testSetTurnRadius_positiveMaxPlusOne >>>>")
    move = Movement()
    move.SetTurnRadius(MAX_POSITIVE_TURN_RADIUS + 1)
    assert move.targetTurnRadius == INFINITE_POSITIVE_TURN_RADIUS


def testSetTurnRadius_negativeMaxMinusOne():
    print(">>>> testSetTurnRadius_negativeMaxMinusOne >>>>")
    move = Movement()
    move.SetTurnRadius(MAX_NEGATIVE_TURN_RADIUS - 1)
    assert move.targetTurnRadius == INFINITE_NEGATIVE_TURN_RADIUS


def testSetTurnRadius_positiveMin():
    print(">>>> testSetTurnRadius_positiveMin >>>>")
    move = Movement()
    move.SetTurnRadius(MIN_POSITIVE_TURN_RADIUS)
    assert move.targetSpeed == 0
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == MIN_POSITIVE_TURN_RADIUS
    assert move.currentTurnRadius == INFINITE_POSITIVE_TURN_RADIUS
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testSetTurnRadius_negativeMin():
    print(">>>> testSetTurnRadius_negativeMin >>>>")
    move = Movement()
    move.SetTurnRadius(MIN_NEGATIVE_TURN_RADIUS)
    assert move.targetSpeed == 0
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == MIN_NEGATIVE_TURN_RADIUS
    assert move.currentTurnRadius == INFINITE_POSITIVE_TURN_RADIUS
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testSetTurnRadius_positiveMinMinusOne():
    print(">>>> testSetTurnRadius_positiveMinMinusOne >>>>")
    move = Movement()
    move.SetTurnRadius(MIN_POSITIVE_TURN_RADIUS - 1)
    # zero radius defaults to positive min value
    assert move.targetTurnRadius == MIN_POSITIVE_TURN_RADIUS


def testSetTurnRadius_negativeMinPlusOne():
    print(">>>> testSetTurnRadius_negativeMinPlusOne >>>>")
    move = Movement()
    move.SetTurnRadius(MIN_NEGATIVE_TURN_RADIUS + 1)
    # zero radius defaults to positive min value
    assert move.targetTurnRadius == MIN_POSITIVE_TURN_RADIUS


def testUpdateAfterSetSpeed_max():
    print(">>>> testUpdateAfterSetSpeed_max >>>>")
    move = Movement()
    move.SetSpeed(MAX_SPEED)
    move.Update()
    assert move.targetSpeed == MAX_SPEED
    assert move.currentSpeed == FW_BW_ACCELERATION
    assert move.targetTurnRadius == INFINITE_POSITIVE_TURN_RADIUS
    assert move.currentTurnRadius == INFINITE_POSITIVE_TURN_RADIUS
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == FW_BW_ACCELERATION
    assert move.rightPwmValue == FW_BW_ACCELERATION


def testUpdateAfterSetSpeed_min():
    print(">>>> testUpdateAfterSetSpeed_min >>>>")
    move = Movement()
    move.SetSpeed(MIN_SPEED)
    move.Update()
    assert move.targetSpeed == MIN_SPEED
    assert move.currentSpeed == -FW_BW_ACCELERATION
    assert move.targetTurnRadius == INFINITE_POSITIVE_TURN_RADIUS
    assert move.currentTurnRadius == INFINITE_POSITIVE_TURN_RADIUS
    assert move.leftDir == GPIO_DIR_BW
    assert move.rightDir == GPIO_DIR_BW
    assert move.leftPwmValue == FW_BW_ACCELERATION
    assert move.rightPwmValue == FW_BW_ACCELERATION


def testUpdateAfterSetTurnRadius_positiveMax():
    print(">>>> testUpdateAfterSetTurnRadius_positiveMax >>>>")
    move = Movement()
    move.SetTurnRadius(MAX_POSITIVE_TURN_RADIUS)
    move.Update()
    assert move.targetSpeed == 0
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == MAX_POSITIVE_TURN_RADIUS
    # Initial turn radius is positive infinite and it should accelerate towards target
    assert move.currentTurnRadius == MAX_POSITIVE_TURN_RADIUS
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testUpdateAfterSetTurnRadius_negativeMax():
    print(">>>> testUpdateAfterSetTurnRadius_negativeMax >>>>")
    move = Movement()
    move.SetTurnRadius(MAX_NEGATIVE_TURN_RADIUS)
    move.Update()
    assert move.targetSpeed == 0
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == MAX_NEGATIVE_TURN_RADIUS
    # Initial turn radius is positive infinite and it should accelerate towards target
    assert move.currentTurnRadius == MAX_NEGATIVE_TURN_RADIUS
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testUpdateAfterSetTurnRadius_positiveMin():
    print(">>>> testUpdateAfterSetTurnRadius_positiveMin >>>>")
    move = Movement()
    move.SetTurnRadius(MIN_POSITIVE_TURN_RADIUS)
    move.Update()
    assert move.targetSpeed == 0
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == MIN_POSITIVE_TURN_RADIUS
    # Initial turn radius is positive infinite and it should accelerate towards target
    assert move.currentTurnRadius == INFINITE_POSITIVE_TURN_RADIUS - TURN_ACCELERATION
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testUpdateAfterSetTurnRadius_negativeMin():
    print(">>>> testUpdateAfterSetTurnRadius_negativeMin >>>>")
    move = Movement()
    move.SetTurnRadius(MIN_NEGATIVE_TURN_RADIUS)
    move.Update()
    assert move.targetSpeed == 0
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == MIN_NEGATIVE_TURN_RADIUS
    # Initial turn radius is positive infinite and it should accelerate towards target
    assert move.currentTurnRadius == INFINITE_NEGATIVE_TURN_RADIUS + TURN_ACCELERATION
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testUpdateAfterSetTurnRadius_positiveStepToZero():
    print(">>>> testUpdateAfterSetTurnRadius_positiveStepToZero >>>>")
    move = Movement()
    move.currentTurnRadius = TURN_ACCELERATION
    move.SetTurnRadius(MIN_POSITIVE_TURN_RADIUS)
    move.Update()
    assert move.targetSpeed == 0
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == MIN_POSITIVE_TURN_RADIUS
    assert move.currentTurnRadius == MIN_POSITIVE_TURN_RADIUS
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testUpdateAfterSetTurnRadius_negativeStepToZero():
    print(">>>> testUpdateAfterSetTurnRadius_negativeStepToZero >>>>")
    move = Movement()
    move.currentTurnRadius = -TURN_ACCELERATION
    move.SetTurnRadius(MIN_NEGATIVE_TURN_RADIUS)
    move.Update()
    assert move.targetSpeed == 0
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == MIN_NEGATIVE_TURN_RADIUS
    assert move.currentTurnRadius == MIN_NEGATIVE_TURN_RADIUS
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testUpdateAfterSetTurnRadius_positiveToNegative():
    print(">>>> testUpdateAfterSetTurnRadius_positiveToNegative >>>>")
    move = Movement()
    move.currentTurnRadius = MAX_POSITIVE_TURN_RADIUS
    move.SetTurnRadius(MAX_NEGATIVE_TURN_RADIUS)
    move.Update()
    assert move.targetSpeed == 0
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == MAX_NEGATIVE_TURN_RADIUS
    assert move.currentTurnRadius == MAX_NEGATIVE_TURN_RADIUS
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testUpdateAfterSetTurnRadius_negativeToPositive():
    print(">>>> testUpdateAfterSetTurnRadius_negativeToPositive >>>>")
    move = Movement()
    move.currentTurnRadius = MAX_NEGATIVE_TURN_RADIUS
    move.SetTurnRadius(MAX_POSITIVE_TURN_RADIUS)
    move.Update()
    assert move.targetSpeed == 0
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == MAX_POSITIVE_TURN_RADIUS
    assert move.currentTurnRadius == MAX_POSITIVE_TURN_RADIUS
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testUpdateAfterSetTurnRadius_positiveToNegativeTargetNotReached():
    print(">>>> testUpdateAfterSetTurnRadius_positiveToNegativeTargetNotReached >>>>")
    move = Movement()
    move.currentTurnRadius = MAX_POSITIVE_TURN_RADIUS
    move.SetTurnRadius(MAX_NEGATIVE_TURN_RADIUS + TURN_ACCELERATION)
    move.Update()
    assert move.targetSpeed == 0
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == MAX_NEGATIVE_TURN_RADIUS + TURN_ACCELERATION
    assert move.currentTurnRadius == MAX_NEGATIVE_TURN_RADIUS + \
        TURN_ACCELERATION - 2  # constant 2 for straight line between pos and neg maxes
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testUpdateAfterSetTurnRadius_negativeToPositiveTargetNotReached():
    print(">>>> testUpdateAfterSetTurnRadius_negativeToPositiveTargetNotReached >>>>")
    move = Movement()
    move.currentTurnRadius = MAX_NEGATIVE_TURN_RADIUS
    move.SetTurnRadius(MAX_POSITIVE_TURN_RADIUS - TURN_ACCELERATION)
    move.Update()
    assert move.targetSpeed == 0
    assert move.currentSpeed == 0
    assert move.targetTurnRadius == MAX_POSITIVE_TURN_RADIUS - TURN_ACCELERATION
    assert move.currentTurnRadius == MAX_POSITIVE_TURN_RADIUS - \
        TURN_ACCELERATION + 2  # constant 2 for straight line between pos and neg maxes
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 0


def testUpdate_SpeedFW_OverWheelToWheelTurnRadiusRight():
    print(">>>> testUpdate_SpeedFW_OverWheelToWheelTurnRadiusRight >>>>")
    move = Movement()
    move.SetSpeed(50)
    move.SetTurnRadius(WHEEL_TO_WHEEL_DIST)
    move.Update()
    assert move.targetSpeed == 50
    assert move.currentSpeed == FW_BW_ACCELERATION
    assert move.targetTurnRadius == WHEEL_TO_WHEEL_DIST
    assert move.currentTurnRadius == INFINITE_POSITIVE_TURN_RADIUS - TURN_ACCELERATION
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == FW_BW_ACCELERATION


def testUpdate_SpeedBW_OverWheelToWheelTurnRadiusRight():
    print(">>>> testUpdate_SpeedBW_OverWheelToWheelTurnRadiusRight >>>>")
    move = Movement()
    move.SetSpeed(-50)
    move.SetTurnRadius(WHEEL_TO_WHEEL_DIST)
    move.Update()
    assert move.targetSpeed == -50
    assert move.currentSpeed == -FW_BW_ACCELERATION
    assert move.targetTurnRadius == WHEEL_TO_WHEEL_DIST
    assert move.currentTurnRadius == INFINITE_POSITIVE_TURN_RADIUS - TURN_ACCELERATION
    assert move.leftDir == GPIO_DIR_BW
    assert move.rightDir == GPIO_DIR_BW
    assert move.leftPwmValue == FW_BW_ACCELERATION


def testUpdate_SpeedFW_OverWheelToWheelTurnRadiusLeft():
    print(">>>> testUpdate_SpeedFW_OverWheelToWheelTurnRadiusLeft >>>>")
    move = Movement()
    move.SetSpeed(50)
    move.SetTurnRadius(-WHEEL_TO_WHEEL_DIST)
    move.Update()
    assert move.targetSpeed == 50
    assert move.currentSpeed == FW_BW_ACCELERATION
    assert move.targetTurnRadius == -WHEEL_TO_WHEEL_DIST
    assert move.currentTurnRadius == INFINITE_NEGATIVE_TURN_RADIUS + TURN_ACCELERATION
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.rightPwmValue == FW_BW_ACCELERATION


def testUpdate_SpeedBW_OverWheelToWheelTurnRadiusLeft():
    print(">>>> testUpdate_SpeedBW_OverWheelToWheelTurnRadiusLeft >>>>")
    move = Movement()
    move.SetSpeed(-50)
    move.SetTurnRadius(-WHEEL_TO_WHEEL_DIST)
    move.Update()
    assert move.targetSpeed == -50
    assert move.currentSpeed == -FW_BW_ACCELERATION
    assert move.targetTurnRadius == -WHEEL_TO_WHEEL_DIST
    assert move.currentTurnRadius == INFINITE_NEGATIVE_TURN_RADIUS + TURN_ACCELERATION
    assert move.leftDir == GPIO_DIR_BW
    assert move.rightDir == GPIO_DIR_BW
    assert move.rightPwmValue == FW_BW_ACCELERATION


def testUpdate_SpeedFW_WheelToWheelTurnRadiusRight():
    print(">>>> testUpdate_SpeedFW_WheelToWheelTurnRadiusRight >>>>")
    move = Movement()
    move.SetSpeed(50)
    move.SetTurnRadius(WHEEL_TO_WHEEL_DIST)
    move.currentTurnRadius = WHEEL_TO_WHEEL_DIST
    move.currentSpeed = 50
    move.Update()
    assert move.targetSpeed == 50
    assert move.currentSpeed == 50
    assert move.targetTurnRadius == WHEEL_TO_WHEEL_DIST
    assert move.currentTurnRadius == WHEEL_TO_WHEEL_DIST
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 50
    assert move.rightPwmValue == 0


def testUpdate_SpeedBW_WheelToWheelTurnRadiusRight():
    print(">>>> testUpdate_SpeedBW_WheelToWheelTurnRadiusRight >>>>")
    move = Movement()
    move.SetSpeed(-50)
    move.SetTurnRadius(WHEEL_TO_WHEEL_DIST)
    move.currentTurnRadius = WHEEL_TO_WHEEL_DIST
    move.currentSpeed = -50
    move.Update()
    assert move.targetSpeed == -50
    assert move.currentSpeed == -50
    assert move.targetTurnRadius == WHEEL_TO_WHEEL_DIST
    assert move.currentTurnRadius == WHEEL_TO_WHEEL_DIST
    assert move.leftDir == GPIO_DIR_BW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 50
    assert move.rightPwmValue == 0


def testUpdate_SpeedFW_WheelToWheelTurnRadiusLeft():
    print(">>>> testUpdate_SpeedFW_WheelToWheelTurnRadiusLeft >>>>")
    move = Movement()
    move.SetSpeed(50)
    move.SetTurnRadius(-WHEEL_TO_WHEEL_DIST)
    move.currentTurnRadius = -WHEEL_TO_WHEEL_DIST
    move.currentSpeed = 50
    move.Update()
    assert move.targetSpeed == 50
    assert move.currentSpeed == 50
    assert move.targetTurnRadius == -WHEEL_TO_WHEEL_DIST
    assert move.currentTurnRadius == -WHEEL_TO_WHEEL_DIST
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 50


def testUpdate_SpeedBW_WheelToWheelTurnRadiusLeft():
    print(">>>> testUpdate_SpeedBW_WheelToWheelTurnRadiusLeft >>>>")
    move = Movement()
    move.SetSpeed(-50)
    move.SetTurnRadius(-WHEEL_TO_WHEEL_DIST)
    move.currentTurnRadius = -WHEEL_TO_WHEEL_DIST
    move.currentSpeed = -50
    move.Update()
    assert move.targetSpeed == -50
    assert move.currentSpeed == -50
    assert move.targetTurnRadius == -WHEEL_TO_WHEEL_DIST
    assert move.currentTurnRadius == -WHEEL_TO_WHEEL_DIST
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_BW
    assert move.leftPwmValue == 0
    assert move.rightPwmValue == 50


def testUpdate_SpeedFW_UnderWheelToWheelTurnRadiusRight():
    print(">>>> testUpdate_SpeedFW_UnderWheelToWheelTurnRadiusRight >>>>")
    move = Movement()
    move.SetSpeed(MAX_SPEED)
    move.SetTurnRadius(WHEEL_TO_WHEEL_DIST / 2)
    move.currentTurnRadius = WHEEL_TO_WHEEL_DIST / 2
    move.currentSpeed = MAX_SPEED
    move.Update()
    assert move.targetSpeed == MAX_SPEED
    assert move.currentSpeed == MAX_SPEED
    assert move.targetTurnRadius == WHEEL_TO_WHEEL_DIST / 2
    assert move.currentTurnRadius == WHEEL_TO_WHEEL_DIST / 2
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_BW
    assert move.leftPwmValue == MAX_SPEED
    assert move.rightPwmValue == MAX_SPEED / 2


def testUpdate_SpeedBW_UnderWheelToWheelTurnRadiusRight():
    print(">>>> testUpdate_SpeedBW_UnderWheelToWheelTurnRadiusRight >>>>")
    move = Movement()
    move.SetSpeed(MIN_SPEED)
    move.SetTurnRadius(WHEEL_TO_WHEEL_DIST / 2)
    move.currentTurnRadius = WHEEL_TO_WHEEL_DIST / 2
    move.currentSpeed = MIN_SPEED
    move.Update()
    assert move.targetSpeed == MIN_SPEED
    assert move.currentSpeed == MIN_SPEED
    assert move.targetTurnRadius == WHEEL_TO_WHEEL_DIST / 2
    assert move.currentTurnRadius == WHEEL_TO_WHEEL_DIST / 2
    assert move.leftDir == GPIO_DIR_BW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == abs(MIN_SPEED)
    assert move.rightPwmValue == abs(MIN_SPEED / 2)


def testUpdate_SpeedFW_UnderWheelToWheelTurnRadiusLeft():
    print(">>>> testUpdate_SpeedFW_UnderWheelToWheelTurnRadiusLeft >>>>")
    move = Movement()
    move.SetSpeed(MAX_SPEED)
    move.SetTurnRadius(-WHEEL_TO_WHEEL_DIST / 2)
    move.currentTurnRadius = -WHEEL_TO_WHEEL_DIST / 2
    move.currentSpeed = MAX_SPEED
    move.Update()
    assert move.targetSpeed == MAX_SPEED
    assert move.currentSpeed == MAX_SPEED
    assert move.targetTurnRadius == -WHEEL_TO_WHEEL_DIST / 2
    assert move.currentTurnRadius == -WHEEL_TO_WHEEL_DIST / 2
    assert move.leftDir == GPIO_DIR_BW
    assert move.rightDir == GPIO_DIR_FW
    assert move.leftPwmValue == MAX_SPEED / 2
    assert move.rightPwmValue == MAX_SPEED


def testUpdate_SpeedBW_UnderWheelToWheelTurnRadiusLeft():
    print(">>>> testUpdate_SpeedBW_UnderWheelToWheelTurnRadiusLeft >>>>")
    move = Movement()
    move.SetSpeed(MIN_SPEED)
    move.SetTurnRadius(-WHEEL_TO_WHEEL_DIST / 2)
    move.currentTurnRadius = -WHEEL_TO_WHEEL_DIST / 2
    move.currentSpeed = MIN_SPEED
    move.Update()
    assert move.targetSpeed == MIN_SPEED
    assert move.currentSpeed == MIN_SPEED
    assert move.targetTurnRadius == -WHEEL_TO_WHEEL_DIST / 2
    assert move.currentTurnRadius == -WHEEL_TO_WHEEL_DIST / 2
    assert move.leftDir == GPIO_DIR_FW
    assert move.rightDir == GPIO_DIR_BW
    assert move.leftPwmValue == abs(MIN_SPEED / 2)
    assert move.rightPwmValue == abs(MIN_SPEED)


#### Test starting point ####

testUpdateBeforeInput()
testSetSpeed_max()
testSetSpeed_maxPlusOne()
testSetSpeed_min()
testSetSpeed_minMinusOne()
testSetTurnRadius_positiveMax()
testSetTurnRadius_negativeMax()
testSetTurnRadius_positiveMaxPlusOne()
testSetTurnRadius_negativeMaxMinusOne()
testSetTurnRadius_positiveMin()
testSetTurnRadius_negativeMin()
testSetTurnRadius_positiveMinMinusOne()
testSetTurnRadius_negativeMinPlusOne()
testUpdateAfterSetSpeed_max()
testUpdateAfterSetSpeed_min()
testUpdateAfterSetTurnRadius_positiveMax()
testUpdateAfterSetTurnRadius_negativeMax()
testUpdateAfterSetTurnRadius_positiveMin()
testUpdateAfterSetTurnRadius_negativeMin()
testUpdateAfterSetTurnRadius_positiveStepToZero()
testUpdateAfterSetTurnRadius_negativeStepToZero()
testUpdateAfterSetTurnRadius_positiveToNegative()
testUpdateAfterSetTurnRadius_negativeToPositive()
testUpdateAfterSetTurnRadius_positiveToNegativeTargetNotReached()
testUpdateAfterSetTurnRadius_negativeToPositiveTargetNotReached()
testUpdate_SpeedFW_OverWheelToWheelTurnRadiusRight()
testUpdate_SpeedBW_OverWheelToWheelTurnRadiusRight()
testUpdate_SpeedFW_OverWheelToWheelTurnRadiusLeft()
testUpdate_SpeedBW_OverWheelToWheelTurnRadiusLeft()
testUpdate_SpeedFW_WheelToWheelTurnRadiusRight()
testUpdate_SpeedBW_WheelToWheelTurnRadiusRight()
testUpdate_SpeedFW_WheelToWheelTurnRadiusLeft()
testUpdate_SpeedBW_WheelToWheelTurnRadiusLeft()
testUpdate_SpeedFW_UnderWheelToWheelTurnRadiusRight()
testUpdate_SpeedBW_UnderWheelToWheelTurnRadiusRight()
testUpdate_SpeedFW_UnderWheelToWheelTurnRadiusLeft()
testUpdate_SpeedBW_UnderWheelToWheelTurnRadiusLeft()

print("==== All Movement module unit tests passed! ====")
