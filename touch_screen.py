# Touch screen functionalities

import enum
import os
import time
from ast import literal_eval
from threading import Thread, Lock
from face_animations import FaceAnimations
from client_socket import ClientSocket
from ip_settings import LOCAL_IP, PORT
from commands import Commands
from logger import PrintToLog

from stub_control import STUB_FACE_RECOGNITION
if STUB_FACE_RECOGNITION:
    from face_recognition_stubs import FaceRecognition
else:
    from face_recognition import FaceRecognition

from stub_control import STUB_PYGAME
if STUB_PYGAME:
    from pygame_stubs import pygame
else:
    import pygame

SCREEN_WIDTH = 480
SCREEN_HEIGHT = 320
UPDATE_PERIOD_S = 0.05
MOUSE_DOWN_DELAY_S = 1
FACE_RECOG_NAMES_SEND_DELAY_S = 10
FOLLOWED_POS_SEND_DELAY_S = 0.1

class TouchScreenModes(enum.Enum):
    FACE = Commands.SCREEN_MODE.value[1][0]
    RECOGNIZER = Commands.SCREEN_MODE.value[1][1]

class TouchScreen:
    def __init__(self):
        self.running = True
        self.mode = TouchScreenModes.FACE
        self.mouseDownTimestampFaceMode = 0
        self.mouseDownTimestampRecogMode = 0
        self.faceRecogNamesSentTimestamp = 0
        self.followedPosSentTimestamp = 0
        self.followedPosName = ""
        self.followedPosNotSeenSent = True

        self.lock = Lock()

        os.putenv('SDL_FBDEV','/dev/fb1')
        os.putenv('SDL_MOUSEDRV', 'TSLIB')
        os.putenv('SDL_MOUSEDEV','/dev/input/event1')

        pygame.display.init()
        size = (SCREEN_WIDTH, SCREEN_HEIGHT)
        self.screen = pygame.display.set_mode(size)
        pygame.mouse.set_visible(False)

        self.faceRecognition = FaceRecognition(SCREEN_WIDTH, SCREEN_HEIGHT)
        self.faceAnimations = FaceAnimations(SCREEN_WIDTH, SCREEN_HEIGHT)
        self.prevFace = self.faceAnimations.GetFace()
        self.tempFaceTimestamp = time.time()
        self.tempFaceDurationS = 0

        self.socket = ClientSocket(LOCAL_IP, PORT)
        self.periodicUpdateThread = Thread(target = self.PeriodicUpdate, args = [])
        self.periodicUpdateThread.start()

        PrintToLog("Touch screen initialized")

    def PeriodicUpdate(self):
        # Check touch events, update screen state and finally draw to the screen.
        while (self.running):
            self.HandleTouchEvents()
            self.UpdateScreenState()
            self.DrawScreen(self.screen)

            time.sleep(UPDATE_PERIOD_S)

    def HandleTouchEvents(self):
        # Poll whether user has touched screen.
        # If yes, check what is screen's mode at the moment and how to
        # react to the touch. Some touch events can just update screen's state
        # directly and some need to send command to main controller via client
        # socket (if something else than screen update is needed)
        if (self.mode == TouchScreenModes.RECOGNIZER):
            self.HandleRecognizerTouchEvents()
        else:
            self.HandleFaceTouchEvents()

    def HandleFaceTouchEvents(self):
        for event in pygame.event.get():
            if (event.type == pygame.MOUSEBUTTONDOWN):
                currentTime = time.time()
                if (currentTime - self.mouseDownTimestampFaceMode > MOUSE_DOWN_DELAY_S):
                    self.mouseDownTimestampFaceMode = currentTime
                    self.socket.SendToServer(Commands.FACE_TOUCHED.value)

    def HandleRecognizerTouchEvents(self):
        for event in pygame.event.get():
            if (event.type == pygame.MOUSEBUTTONDOWN):
                currentTime = time.time()
                if (currentTime - self.mouseDownTimestampRecogMode > MOUSE_DOWN_DELAY_S):
                    self.mouseDownTimestampRecogMode = currentTime
                    self.socket.SendToServer(Commands.RECOG_TOUCHED.value)

    def UpdateScreenState(self):
        currentTime = time.time()
        if (self.mode == TouchScreenModes.RECOGNIZER):
            self.faceRecognition.Update()
            names, positions = self.faceRecognition.GetRecognizedObjects()
            if ((len(names) > 0) and (currentTime - self.faceRecogNamesSentTimestamp > FACE_RECOG_NAMES_SEND_DELAY_S)):
                namesStr = repr(names)
                PrintToLog("Send names to server: " + namesStr)
                self.socket.SendToServer(Commands.RECOG_OBJECTS.value + "#" + namesStr)
                self.faceRecogNamesSentTimestamp = currentTime

            followedObjSeen = False
            if (self.followedPosName != ""):
                i = 0
                for name in names:
                    if ((name == self.followedPosName) and (currentTime - self.followedPosSentTimestamp > FOLLOWED_POS_SEND_DELAY_S)):
                        self.followedPosSentTimestamp = currentTime
                        followedObjSeen = True
                        self.followedPosNotSeenSent = False
                        posStr = repr(positions[i])
                        PrintToLog("Send pos to server: " + posStr)
                        self.socket.SendToServer(Commands.RECOG_OBJECT_POS.value + "#" + posStr)

                    i += 1

            if (followedObjSeen == False and self.followedPosNotSeenSent == False):
                self.followedPosNotSeenSent = True
                self.socket.SendToServer(Commands.RECOG_OBJECT_POS.value + "#(-1,-1)")

        else:
            self.lock.acquire()
            if ((self.tempFaceDurationS >= 0) and (currentTime - self.tempFaceTimestamp > self.tempFaceDurationS)):
                self.faceAnimations.SetFace(self.prevFace[0], self.prevFace[1])
                self.tempFaceDurationS = -1
            self.lock.release()

            self.faceAnimations.Update()

    def DrawScreen(self, screen):
        if (self.mode == TouchScreenModes.RECOGNIZER):
            self.faceRecognition.Draw(screen)
        else:
            self.faceAnimations.Draw(screen)

        pygame.display.update()

######## Below functions are meant to be called outside of this module ########

    def SendPosData(self, objectName):
        self.followedPosName = objectName

    def SetMode(self, newMode):
        # Image recognition mode or animated face
        for mode in TouchScreenModes:
            if mode.value == newMode:
                self.mode = mode
                break

    def GetMode(self):
        return self.mode.value

    def SetFace(self, faceEmotion, faceAction, durationS = -1):
        # What kind of face to draw
        if (durationS < 0):
            self.lock.acquire()
            self.tempFaceDurationS = durationS
            self.faceAnimations.SetFace(faceEmotion, faceAction)
            self.lock.release()
        else:
            self.lock.acquire()
            self.prevFace = self.faceAnimations.GetFace()
            self.faceAnimations.SetFace(faceEmotion, faceAction)
            self.tempFaceTimestamp = time.time()
            self.tempFaceDurationS = durationS
            self.lock.release()

    def SetFaceBubble(self, bubbleEnum):
        # What kind of action bubble to draw next to face
        self.faceAnimations.SetBubble(bubbleEnum)

    def Cleanup(self):
        self.running = False
        self.periodicUpdateThread.join()
        self.faceAnimations.Cleanup()
        self.faceRecognition.Cleanup()
        pygame.quit()
        self.socket.Cleanup()
