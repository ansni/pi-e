# General info:
Robot build around Raspberry Pi 3B board.

## Features:
- Conversation:
    * Speech recognition
	* Machine learning based dialog
	* Voice synthesis
- Expression animation:
    * Speaking, happy, unhappy, tired
	* Activated by conversation and touch
- Machine vision:
    * Object recognition with webcam
- Movement:
    * Tracks for moving in all directions
	* Object following based on machine vision
- Remote control:
    * Remote control from Android mobile app
	* Movement with a virtual joystick
	* Speaking out user's text input
	* "Random" button :)

# HW info:
- Raspberry Pi 3B
- USB webcam with microphone
- 2x LEDs at front
- 2x 12V DC motors
- 2x motor driver H bridge board L298N
- 12V to 5V DC-DC buck step down voltage regulator board
- 12V Li-ion battery
- 3,5" TFT touch screen with SPI interface. 26-pin header.
- Push button
- Power switch
- Speakers with 3,5mm plug connector
- Case around Raspberry Pi and TFT screen
- Robot chassis with tracks

## Pinout
- See gpio_pin_mapping.py for GPIO pin mapping

# SW info:
- Raspbian OS
- Written with Python

## 3rd party libraries
- Hotword detection: Snowboy (https://snowboy.kitt.ai/). -- Apache licensed.
- Speech to text: Google Cloud (https://cloud.google.com/speech-to-text). -- Commercial. Personal access token "google_access_token.json" needed.
- Text to speech: eSpeak (http://espeak.sourceforge.net/). -- GPLv3 licensed.
- AI chat bot core: Wit.ai (https://wit.ai/) -- Licensed for free use. Personal access token needed "wit_access_token.txt".
- Object detection: OpenCV (https://opencv.org/). -- BSD 3-Clause License.
- Display drawing and touch recognition: Pygame (https://www.pygame.org/). -- LGPL licensed.
- Mobile application's joystick part: Copied from https://github.com/efficientisoceles/JoystickView. -- GPLv3 licensed.
- Robot face graphics: (Purchased from https://www.istockphoto.com/fi/vector/set-techno-emotions-to-create-characters-gm1140828896-305420323)

### Supported but not used
Google Cloud libraries for speech-to-text and text-to-speech conversion work well but are proprietary software. You will need your own account and API key if you want to modify the code to use these.
This is not needed as by default the Pi-E project uses open source alternatives for speech-to-text and text-to-speech conversions.
To enable the Google libraries create an API key json file in Google's webpage (https://cloud.google.com/) and store it in pi-e/ folder. Modify speech_to_text_google.py and text_to_speech_google.py to read the new file.
Also modify main_control.py to import *_google.py files instead of the open source alternatives.

# Build instructions:
- TODO: these are not accurate instructions
- Install minimal headless Raspbian OS
- Install the 3rd party libraries
- Clone Pi-E repository
- Modify /etc/rc.local to have line TODO. This makes the Pi-E program to start automatically at bootup.

# Usage instructions:
- Activate voice recognition by saying "Heippa" to it or by keeping down microphone push button. Voice recognition stays active few seconds indicated by front LEDs.
- Supported voice commands are:
    * "Kamera": switches to object recognition mode
	* "Naama": switches to expression animation mode
    * "Seuraa": the robot starts to follow a human it sees
	* "Odota": the robot stops following humans
	* "Nukkumaan": the robot shuts itself down
- If speech is not recognized to be one of the above voice commands, the robot responds but won't do any additional actions.

