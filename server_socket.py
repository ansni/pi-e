import socket

class ServerSocket:
    '''Helper class to use create and use a server socket'''

    def __init__(self, ip, port, timeout):
        self.ip = ip
        self.port = port

        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP socket
        self.s.bind((ip, port))
        self.s.settimeout(timeout)  # in seconds

    def WaitForData(self):
        dataStr = ""
        try:
            data, addr = self.s.recvfrom(256)
        except socket.timeout:
            pass

        dataStr = data.decode("utf-8")
        return dataStr

    def Cleanup(self):
        self.s.close()
