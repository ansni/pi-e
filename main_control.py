#!/usr/bin/python3

# Top level control of actuators based on inputs gotten from server socket

from stub_control import SetupStubDir
SetupStubDir()

import signal
import subprocess
from ast import literal_eval
from ip_settings import LOCAL_IP, PORT
from time import sleep
from logger import PrintToLog
from server_socket import ServerSocket
from client_socket import ClientSocket
from movement import Movement
from touch_screen import TouchScreen, TouchScreenModes
from face_animations import FaceEmotions, FaceActions, BubbleIcons
from speech_to_text_google import SpeechToText
from text_to_speech_espeak import TextToSpeech
from microphone import Microphone
from pushbutton import Pushbutton
from hotword_detector import HotwordDetector
from led_control import LedControl, LedStates, LedSelection
from commands import Commands, SpeechCommands

# for simulating GPIO input through socket interface and setting GPIO mode
from stub_control import STUB_GPIO
if STUB_GPIO:
    import GPIO_stubs as GPIO
else:
    import RPi.GPIO as GPIO

from stub_control import STUB_AI
if STUB_AI:
    from ai_stubs import AI, AIModes
else:
    from ai import AI, AIModes

from stub_control import STUB_SOUND_OUT
if STUB_SOUND_OUT:
    from sound_out_stubs import SoundOut
else:
    from sound_out import SoundOut

def SignalHandler(signum, frame):
        PrintToLog("Signal handler called with signal: " + str(signum))

        # Make the app shut down nicely
        raise KeyboardInterrupt

class MainControl:
    def __init__(self):
        self.shutdownPending = False
        self.busy = False
        self.ai = AI()
        self.ledControl = LedControl()
        self.microphone = Microphone()
        self.move = Movement()
        self.speechToText = SpeechToText("fi")
        self.textToSpeech = TextToSpeech("fi")
        self.soundOut = SoundOut()
        self.touchScreen = TouchScreen()
        self.pushbutton = Pushbutton()
        self.hotwordDetector = HotwordDetector()
        self.clientSocket = ClientSocket(LOCAL_IP, PORT)

    def HandleInput(self, strData):
        status = "ok"

        cmdList = strData.split("#")
        cmdType = cmdList[0]
        PrintToLog("cmdType: " + cmdType)

        if (cmdType == Commands.FACE.value[0]):
            if (len(cmdList) == 4):
                faceEmotion = cmdList[1]
                faceAction = cmdList[2]
                duration = int(cmdList[3])
                self.touchScreen.SetFace(faceEmotion, faceAction, duration)
            else:
                PrintToLog("Invalid len in face cmd: " + str(len(cmdList)))
                status = "error"

        elif (cmdType == Commands.SCREEN_MODE.value[0]):
            if (len(cmdList) == 2):
                mode = cmdList[1]
                self.touchScreen.SetMode(mode)
            else:
                PrintToLog("Invalid len in screen_mode cmd " + str(len(cmdList)))
                status = "error"

        elif (cmdType == Commands.FACE_TOUCHED.value):
            self.touchScreen.SetFaceBubble(BubbleIcons.Question)
            self.ai.ResponseToFaceTouch(self.AIResponseReady)

        elif (cmdType == Commands.RECOG_TOUCHED.value):
            self.ai.ResponseToRecogTouch(self.AIResponseReady)

        elif (cmdType == Commands.RECOG_OBJECTS.value):
            if (len(cmdList) == 2):
                PrintToLog("Recog objects data from socket: " + cmdList[1])
                namesList = literal_eval(cmdList[1])
                self.ai.ResponseToObjectRecog(namesList, self.AIResponseReady)
            else:
                PrintToLog("Invalid len in recog-names cmd: " + str(len(cmdList)))
                status = "error"

        elif (cmdType == Commands.RECOG_OBJECT_POS.value):
            if (len(cmdList) == 2):
                pos = literal_eval(cmdList[1])
                PrintToLog("Recog object pos data from socket: " + cmdList[1])
                self.ai.UpdateFollowedObjectPosition(pos)
            else:
                PrintToLog("Invalid len in recog-object-pos cmd: " + str(len(cmdList)))
                status = "error"

        elif (cmdType == Commands.MOVE.value):
            if (len(cmdList) == 3):
                speed = int(cmdList[1])
                turnRadius = int(cmdList[2])
                self.move.SetSpeed(speed)
                self.move.SetTurnRadius(turnRadius)
            else:
                PrintToLog("Invalid len in move cmd: " + str(len(cmdList)))
                status = "error"

        elif (cmdType == Commands.IMMEDIATE_STOP.value):
            self.move.Stop()

        elif (cmdType == Commands.SPEAK.value):
            if (len(cmdList) == 2):
                textToSpeak = cmdList[1]
                self.ledControl.SetFrontLedStates(LedStates.Pulsating)
                self.textToSpeech.StartConversion(textToSpeak, self.TextToSpeechConversionReady)
            else:
                PrintToLog("Invalid len in speak cmd: " + str(len(cmdList)))
                status = "error"

        elif (cmdType == Commands.MIC_SIM.value):
            if (len(cmdList) == 2):
                # fake voice commands with text
                self.SpeechToTextConversionReady(cmdList[1])
            else:
                PrintToLog("Invalid len in mic-sim cmd: " + str(len(cmdList)))
                status = "error"

        elif (cmdType == Commands.BTN_LONG_PRESS.value):
            if (len(cmdList) == 1):
                # long button press event. Do graceful shutdown.
                self.shutdownPending = True
                self.touchScreen.SetFaceBubble(BubbleIcons.Speech)
                self.textToSpeech.StartConversion(SpeechCommands.SHUTDOWN.value[1], self.TextToSpeechConversionReady)
            else:
                PrintToLog("Invalid len in btn-long-press cmd: " + str(len(cmdList)))
                status = "error"

        elif ((cmdType == Commands.BTN_SHORT_PRESS.value) or (cmdType == Commands.HOTWORD.value)):
            if (len(cmdList) == 1):
                # short button press event or hotword detected. Start recording sound.
                if (not self.busy):
                    PrintToLog("Starting sound recording")
                    self.busy = True
                    self.ledControl.SetFrontLedStates(LedStates.On)
                    self.touchScreen.SetFaceBubble(BubbleIcons.Listening)
                    self.microphone.RecordSound(self.SoundRecordingReady)
                else:
                    PrintToLog("Rejected sound recording. Busy.")
            else:
                PrintToLog("Invalid len in btn-short-press/hotword cmd: " + str(len(cmdList)))
                status = "error"

        elif (cmdType == Commands.BTN_DOUBLE_PRESS.value):
            if (len(cmdList) == 1):
                # double button press event. Speak out supported commands.
                if (not self.busy):
                    PrintToLog("Telling supported commands")
                    self.busy = True
                    self.touchScreen.SetFaceBubble(BubbleIcons.Speech)
                    self.textToSpeech.StartConversion(SpeechCommands.ToString(), self.TextToSpeechConversionReady)
                else:
                    PrintToLog("Rejected telling commands. Busy.")
            else:
                PrintToLog("Invalid len in btn-short-press/hotword cmd: " + str(len(cmdList)))
                status = "error"

        elif (cmdType == Commands.RANDOM.value):
            # Do a random operation
            self.ai.ResponseRandomly(self.AIResponseReady)

        elif (cmdType == Commands.EXIT.value):
            if (len(cmdList) == 1):
                # Do shutdown.
                status = "exit"
            else:
                PrintToLog("Invalid len in exit cmd: " + str(len(cmdList)))
                status = "error"

        elif (STUB_GPIO and (cmdType == Commands.GPIO_SIM_IN.value)):
            if (len(cmdList) == 3):
                pin = cmdList[1]
                value = int(cmdList[2])
                GPIO.SIMULATED_INPUT_VALUE[pin] = value
            else:
                PrintToLog("Invalid len in gpio sim in cmd: " + str(len(cmdList)))
                status = "error"

        else:
            PrintToLog("Unknown cmd: " + str(cmdType))
            status = "error"

        return status

    def SoundRecordingReady(self, soundFilePath):
        PrintToLog("Sound recording ready. Starting speech-to-text conversion")
        self.ledControl.SetFrontLedStates(LedStates.Pulsating)
        self.touchScreen.SetFaceBubble(BubbleIcons.Thinking)
        self.speechToText.StartConversion(soundFilePath, self.SpeechToTextConversionReady)

    def SpeechToTextConversionReady(self, conversionStr):
        PrintToLog("Speech-to-text conversion ready")
        PrintToLog("Converted text: " + conversionStr)

        if (conversionStr == SpeechCommands.CAMERA.value[0]):
            self.touchScreen.SetMode(TouchScreenModes.RECOGNIZER.value)

        elif (conversionStr == SpeechCommands.FACE.value[0]):
            self.touchScreen.SetMode(TouchScreenModes.FACE.value)

        elif (conversionStr == SpeechCommands.FOLLOW.value[0]):
            self.textToSpeech.StartConversion(SpeechCommands.FOLLOW.value[1], self.TextToSpeechConversionReady)                                    
            self.ai.SetToMode(AIModes.Follow)
            self.touchScreen.SendPosData("ihminen")

        elif (conversionStr == SpeechCommands.WAIT.value[0]):
            self.textToSpeech.StartConversion(SpeechCommands.WAIT.value[1], self.TextToSpeechConversionReady)
            self.ai.SetToMode(AIModes.Stationary)
            self.touchScreen.SendPosData("")

        elif (conversionStr == SpeechCommands.SHUTDOWN.value[0]):
            self.shutdownPending = True
            self.textToSpeech.StartConversion(SpeechCommands.SHUTDOWN.value[1], self.TextToSpeechConversionReady)

        elif (conversionStr != ""):
            PrintToLog("Starting AI response calculation")
            self.ai.ResponseTo(conversionStr, self.AIResponseReady)
        else:
            # User hasn't said anything. No need to process sound input futher.
            PrintToLog("User input speech empty. Giving random response")
            self.ai.ResponseRandomly(self.AIResponseReady)

    def AIResponseReady(self, responseStr):
        PrintToLog("AI response ready")
        PrintToLog("Response: " + responseStr)
        PrintToLog("Starting text-to-speech conversion")

        self.textToSpeech.StartConversion(responseStr, self.TextToSpeechConversionReady)

    def TextToSpeechConversionReady(self, soundFilePath):
        PrintToLog("Text-to-speech conversion ready")
        PrintToLog("Starting sound producing")

        self.ledControl.SetFrontLedStates(LedStates.Toggling)
        self.touchScreen.SetFaceBubble(BubbleIcons.Speech)

        if (self.touchScreen.GetMode() == TouchScreenModes.FACE.value):
            self.touchScreen.SetFace(Commands.FACE.value[1][0], Commands.FACE.value[2][1])

        self.soundOut.Play(soundFilePath, self.SoundPlayFinished)

    def SoundPlayFinished(self):
        PrintToLog("Sound play finished")

        self.ledControl.SetFrontLedStates(LedStates.Off)
        self.touchScreen.SetFaceBubble(BubbleIcons.Nothing)

        if (self.touchScreen.GetMode() == TouchScreenModes.FACE.value):
            self.touchScreen.SetFace(Commands.FACE.value[1][0], Commands.FACE.value[2][0])

        if (self.shutdownPending):
            self.clientSocket.SendToServer(Commands.EXIT.value)
        else:
            # Fake hotword detection so sound recording will start again automatically
            self.busy = False

            # Commented out for now until conversiation is debugged to work smoothly.
            #self.clientSocket.SendToServer(Commands.HOTWORD.value)

    def Cleanup(self):
        self.move.Cleanup()
        self.soundOut.Cleanup()
        self.touchScreen.Cleanup()
        self.speechToText.Cleanup()
        self.textToSpeech.Cleanup()
        self.microphone.Cleanup()
        self.ai.Cleanup()
        self.ledControl.Cleanup()
        self.pushbutton.Cleanup()
        self.hotwordDetector.Cleanup()
        self.clientSocket.Cleanup()


######## Starting point for main control software ########

CHECK_INPUT_PERIOD_S = 0.01

PrintToLog("Log started", init=True)

signal.signal(signal.SIGUSR1, SignalHandler)

# GPIO is used by multple modules so initialize & deinitialize it here in the main module
GPIO.setmode(GPIO.BCM)  # We are using the BCM pin numbering

s = ServerSocket(ip="", port=PORT, timeout=None)
mc = MainControl()

fullShutdown = False
try:
    running = True
    while running:
        data = s.WaitForData()
        status = mc.HandleInput(data)
        if status == "exit":
            running = False
            fullShutdown = True
        else:
            sleep(CHECK_INPUT_PERIOD_S) #sleep a while to ensure other threads get execution time

except KeyboardInterrupt:
    PrintToLog("Keyboard interrupt received, exiting")

# Cleanup before exiting
mc.Cleanup()
s.Cleanup()

GPIO.cleanup()  # Make all the output pins LOW

PrintToLog("All cleaned up")

if fullShutdown:
    subprocess.run(["shutdown", "now"])
