# Sound output module

import os
import subprocess
from threading import Thread
from queue import Queue
from time import sleep
from logger import PrintToLog

UPDATE_PERIOD_S = 0.5

class QItem:
    def __init__(self, soundFilePath, Callback):
        self.soundFilePath = soundFilePath
        self.Callback = Callback

class SoundOut:
    def __init__(self):
        self.queue = Queue()
        self.playThread = Thread(target = self.PlayWorker, args = [])
        self.playThread.start()
        PrintToLog("Sound out initialized")

    def Play(self, soundFilePath, FinishedCallback):
        item = QItem(soundFilePath, FinishedCallback)
        self.queue.put(item)

    def PlayWorker(self):

        while True:
            item = self.queue.get()
            if item is None:
                break; # exit thread

            subprocess.run(["omxplayer", "--no-keys", "-o", "local", item.soundFilePath])
            item.Callback()

            sleep(UPDATE_PERIOD_S) # prevent CPU exhaustion if queue has lots of items

    def Cleanup(self):
        PrintToLog("Sound module cleanup")
        self.queue.put(None)
        self.playThread.join()
