# Microphone recording

from threading import Thread
from logger import PrintToLog
import queue
import time
import subprocess
import os

UPDATE_PERIOD_S = 0.5
AUDIO_DURATION_S = 5

THIS_FILE_DIR = os.path.dirname(os.path.realpath(__file__))
VOICE_FILE_NAME = "recorded_sound.wav"
VOICE_FILE_PATH = THIS_FILE_DIR + "/" + VOICE_FILE_NAME

class QItem:
    def __init__(self, Callback):
        self.Callback = Callback

class Microphone:
    def __init__(self):
        self.reqQueue = queue.Queue()
        self.recordingThread = Thread(target = self.RecordingWorker, args = [])
        self.recordingThread.start()

        PrintToLog("Microphone initialized")

    def RecordingWorker(self):
        # Does microphone recording when requested and calls requester callback when ready

        while True:

            item = self.reqQueue.get()
            if item is None:
                break #exit thread

            recordProcess = subprocess.run(["arecord", "--rate", "16000", "--format", "S16_LE", "--file-type", "wav", "--duration", str(AUDIO_DURATION_S), VOICE_FILE_PATH])

            # Call requester callback with recorded sound file path
            item.Callback(VOICE_FILE_PATH)

            time.sleep(UPDATE_PERIOD_S) # Prevent CPU exhaustion if there are lots of requests

    def RecordSound(self, RecordingReadyCallback):
        item = QItem(RecordingReadyCallback)
        self.reqQueue.put(item)

    def Cleanup(self):
        self.reqQueue.put(None)
        self.recordingThread.join()
        PrintToLog("Microphone cleanup")
