# Converts text to voice file

from threading import Thread
from queue import Queue
from time import sleep
from logger import PrintToLog
import os
import subprocess

VOICE_AMPLITUDE = "70"
VOICE_PITCH = "40"
VOICE_SPEED = "190"

THIS_FILE_DIR = os.path.dirname(os.path.realpath(__file__))
SOUND_FILE_PATH = THIS_FILE_DIR + "/sound_to_play.wav"

UPDATE_PERIOD_S = 0.5

class QItem:
    def __init__(self, text, Callback):
        self.text = text
        self.Callback = Callback

class TextToSpeech:
    def __init__(self, languageCode):
        #languageCode not needed with espeak but kept as parameter for interface compatibility

        self.queue = Queue()
        self.conversionThread = Thread(target = self.ConversionWorker, args = [])
        self.conversionThread.start()

        PrintToLog("Text-to-speech initialized")

    def StartConversion(self, textStr, ConversionReadyCallbackFunc):
        item = QItem(textStr, ConversionReadyCallbackFunc)
        self.queue.put(item)

    def ConversionWorker(self):

        while True:

            item = self.queue.get()
            if item is None:
                break # exit thread

            subprocess.run(["espeak",
                            "-a", VOICE_AMPLITUDE,
                            "-p", VOICE_PITCH,
                            "-s", VOICE_SPEED,
                            "-v", "finnish",
                            "-w", SOUND_FILE_PATH,
                            item.text])

            item.Callback(SOUND_FILE_PATH)

            sleep(UPDATE_PERIOD_S) #prevent CPU exhaustion if queue has lots of items

    def Cleanup(self):
        self.queue.put(None)
        self.conversionThread.join()
        PrintToLog("Text-to-speech cleanup")
