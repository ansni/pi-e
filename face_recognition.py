# Face & object recognition module.
# Detects and marks objects in webcam's video stream.

import pygame
import cv2
import numpy as np
import sys
import os
from logger import PrintToLog
from threading import Lock

thisFileDir = os.path.dirname(os.path.realpath(__file__))

# Pretrained classes in the model
classNames = {0: 'tausta',
              1: 'ihminen', 2: 'polkupyörä', 3: 'auto', 4: 'moottoirpyörä', 5: 'lentokone', 6: 'bussi',
              7: 'juna', 8: 'rekka', 9: 'vene', 10: 'liikennevalo', 11: 'vesiposti',
              13: 'stop-merkki', 14: 'parkkimittari', 15: 'penkki', 16: 'lintu', 17: 'kissa',
              18: 'koira', 19: 'hevonen', 20: 'lammas', 21: 'lehma', 22: 'norsu', 23: 'karhu',
              24: 'seepra', 25: 'kirahvi', 27: 'reppu', 28: 'sateenvarjo', 31: 'käsilaukku',
              32: 'solmio', 33: 'matkalaukku', 34: 'frisbii', 35: 'sukset', 36: 'lumilauta',
              37: 'pallo', 38: 'leija', 39: 'pesäpallomaila', 40: 'pesäpalloräpylä',
              41: 'rullalauta', 42: 'surffilauta', 43: 'tennismaila', 44: 'pullo',
              46: 'viinilasi', 47: 'kuppi', 48: 'haarukka', 49: 'veitsi', 50: 'lusikka',
              51: 'kulho', 52: 'banaani', 53: 'omena', 54: 'voileipa', 55: 'appelsiini',
              56: 'parsakaali', 57: 'porkkana', 58: 'hot dog', 59: 'pizza', 60: 'donitsi',
              61: 'kakku', 62: 'tuoli', 63: 'sohva', 64: 'ruukkukasvi', 65: 'sänky',
              67: 'ruokapöytä', 70: 'vessa', 72: 'televisio', 73: 'läppäri', 74: 'hiiri',
              75: 'kaukosäädin', 76: 'näppäimisto', 77: 'kännykkä', 78: 'mikro', 79: 'uuni',
              80: 'leivänpaahdin', 81: 'pesuallas', 82: 'jääkaappi', 84: 'kirja', 85: 'kello',
              86: 'vaasi', 87: 'sakset', 88: 'nalle', 89: 'hiustenkuivaaja', 90: 'hammasharja'}

class FaceRecognition:
    def __init__(self, screenWidth, screenHeight):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight

        self.lock = Lock()

        # Loading model
        self.model = cv2.dnn.readNetFromTensorflow(thisFileDir + '/face_recognition_models/frozen_inference_graph.pb',
                                      thisFileDir + '/face_recognition_models/ssd_mobilenet_v2_coco_2018_03_29.pbtxt')
        self.camera = cv2.VideoCapture(0)
        self.frame = None
        self.recognizedObjects = []
        self.recognizedPositions = []

    def Update(self):
        ret, self.frame = self.camera.read()

        # Perform object detection. Neural network expects size 300x300
        self.model.setInput(cv2.dnn.blobFromImage(self.frame, size=(300, 300), swapRB=True))
        output = self.model.forward()

        self.lock.acquire() # lock for accessing names list that is accessible from API functions also
        self.recognizedObjects.clear()
        self.recognizedPositions.clear()
        for detection in output[0, 0, :, :]:
            confidence = detection[2]
            if confidence > .5:
                class_id = detection[1]
                class_name = classNames[class_id]
                box_x = detection[3] * self.screenWidth
                box_y = detection[4] * self.screenHeight
                box_width = detection[5] * self.screenWidth
                box_height = detection[6] * self.screenHeight

                x_center_pos = detection[3] + (detection[5] / 2.0) #scale from 0.0 to 1.0
                x_center_pos = int(x_center_pos * 100.0) #scale from 0 to 100
                y_center_pos = detection[4] + (detection[6] / 2.0) #scale from 0.0 to 1.0
                y_center_pos = int(y_center_pos * 100.0) #scale from 0 to 100
                self.recognizedObjects.append(class_name)
                self.recognizedPositions.append((x_center_pos, y_center_pos))

                cv2.rectangle(self.frame, (int(box_x), int(box_y)), (int(box_width), int(box_height)), (23, 230, 210), thickness=2)
                cv2.putText(self.frame, class_name ,(int(box_x), int(box_y+.17*self.screenHeight)), cv2.FONT_HERSHEY_SIMPLEX, (.0035*self.screenWidth), (0, 0, 255), thickness=2)

        self.lock.release()

    def GetRecognizedObjects(self):
        self.lock.acquire()
        names = self.recognizedObjects
        positions = self.recognizedPositions
        self.lock.release()

        return names, positions

    def Draw(self, screen):
        if not self.frame is None:
            self.frame = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)
            self.frame = self.frame.swapaxes(0,1)
            self.frame = pygame.surfarray.make_surface(self.frame)
            screen.blit(self.frame, (0,0))

    def Cleanup(self):
        cv2.destroyAllWindows()
