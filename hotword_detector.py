# Detects pre-defined hotword from microphone audio using Snowboy library

import os
import sys
from threading import Thread
from logger import PrintToLog
from commands import Commands
from client_socket import ClientSocket
from ip_settings import LOCAL_IP, PORT

THIS_FILE_DIR = os.path.dirname(os.path.realpath(__file__))
MODEL_FILE_NAME = "hotword_heippa.pmdl"
MODEL_FILE_PATH = THIS_FILE_DIR + "/" + MODEL_FILE_NAME

SNOWBOY_MODULE_PATH = THIS_FILE_DIR + "/../snowboy/bin/rpi-arm-raspbian-8.0-1.1.1-python3"
sys.path.insert(0, SNOWBOY_MODULE_PATH)
import snowboydecoder

class HotwordDetector:
    def __init__(self):
        self.socket = ClientSocket(LOCAL_IP, PORT)
        self.detector = snowboydecoder.HotwordDetector(MODEL_FILE_PATH, sensitivity=0.5, audio_gain=1)
        self.exit = False
        self.hotwordThread = Thread(target = self.HotwordWorker, args = [])
        self.hotwordThread.start()
        PrintToLog("Hotword detector initialized")

    def HotwordWorker(self):
        self.detector.start(detected_callback=self.DetectedCallback,
                            interrupt_check=self.IsExitPending,
                            sleep_time=0.03)

    def IsExitPending(self):
        return self.exit

    def DetectedCallback(self):
        # commented out for now until conversation in debugged to work smoothly. For now trigger conversation from button
        #self.socket.SendToServer(Commands.HOTWORD.value)
        pass

    def Cleanup(self):
        self.exit = True
        self.hotwordThread.join()
        self.detector.terminate()
        self.socket.Cleanup()
        PrintToLog("Hotword cleanup")
